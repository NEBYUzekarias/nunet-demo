import unittest
import logging
import organizer
import time
import sys
import docker
import grpc
import object_detection_pb2_grpc
import object_detection_pb2

import image_recon_pb2_grpc
import image_recon_pb2
import base64
import random

import json
import ast
import requests

from constants import CONTAINER_NAME

class TestOrchestrator(unittest.TestCase):
    orch = None

    @classmethod
    def setUpClass(self):
        self.orch = organizer.Orchestrator()

    @classmethod
    def tearDownClass(self):
        self.orch = None

    def setUp(self):
        self.yolo_arg = ["python3", "-m", "service.object_detection_service", "--grpc-port","8889"]
        self.yolo_img="yolov3-object-detection"

        self.cntk_arg = ["/root/anaconda3/envs/cntk-py35/bin/python", "-m", "service.image_recon_service", "--grpc-port","8890"]
        self.cntk_img="cntk-image-recon"
        self.img_cntk=base64.b64encode(requests.get("https://singnet.github.io/dnn-model-services/assets/users_guide/bulldog.jpg").content)
        self.img_cntk2 = base64.b64encode(requests.get("https://raw.githubusercontent.com/singnet/dnn-model-services/master/docs/assets/users_guide/rose.jpg").content)
        self.img = "dog.jpeg"
        self.img2 = "rose.jpg"
        self.__class__.net = self.orch.client.networks.get(CONTAINER_NAME)

    def test_a_yolov3_creator(self):
        self.__class__.yolov_cont, self.__class__.yolov_ip = self.orch.create(self.__class__.net.id, self.yolo_img, self.yolo_arg)
        self.assertEqual('created', self.__class__.yolov_cont.status)

    def test_b_cntk_creator(self):
        self.__class__.cntk_cont, self.__class__.cntk_ip = self.orch.create(self.__class__.net.id,self.cntk_img, self.cntk_arg)
        self.assertEqual('created', self.__class__.cntk_cont.status)

    def test_c_yollo_call(self):
        yolo_file="file"
        stat = {
           "memory_limit": 10,
           "time_taken": 10,
           "memory_usage": 10, # is not memory percentage
           "net_rx": 10,
           "net_tx": 10,
           "cpu_usage":10,  # is not cpu percentage
           "total_memory_usage":10
          } 
        with open("file.txt","w") as f:
            f.write('{'+"'"+str(10)+"'"+":"+str(stat))

        # for detected dog image
        yolo=self.orch.call_yolo_cont(self.__class__.yolov_ip, self.__class__.yolov_cont, 8889, self.img, "test",yolo_file)

        for bool in yolo:
            self.assertTrue(bool)

        yolov_cont,yolov_ip = self.orch.create(self.__class__.net.id, self.yolo_img, self.yolo_arg)
        # no dog in the pic
        with open("file.txt","w") as f:
            f.write('{'+"'"+str(10)+"'"+":"+str(stat))
        yolo2=self.orch.call_yolo_cont(yolov_ip, yolov_cont, 8889, self.img2, "test",yolo_file)

        for bool in yolo2:
            self.assertNotEqual(True,bool)

    def test_d_cntk_call(self):
        # for detected dog image
        cntk_file="file"
        stat = {
           "memory_limit": 10,
           "time_taken": 10,
           "memory_usage": 10, # is not memory percentage
           "net_rx": 10,
           "net_tx": 10,
           "cpu_usage":10,  # is not cpu percentage
           "total_memory_usage":10
          } 
        with open("file.txt","w") as f:
            f.write('{'+"'"+str(3)+"'"+":"+str(stat))

        cntk = self.orch.call_cntk_cont(self.__class__.cntk_ip, self.__class__.cntk_cont, 8890, self.img_cntk,cntk_file)
        buldog = []
        cnt = 0
        print(cntk)
        for tag, log, res in cntk:
            if cnt==2:
               buldog.append(log)
            cnt=cnt+1
        self.assertTrue('Bulldog' in str(buldog[0]))



    def test_e_delete(self):
        cntk_cont, cntk_ip = self.orch.create(self.__class__.net.id,self.cntk_img, self.cntk_arg)
        yolov_cont,yolov_ip = self.orch.create(self.__class__.net.id, self.yolo_img, self.yolo_arg)
        self.orch.delete(yolov_cont, self.__class__.net)
        self.orch.delete(cntk_cont, self.__class__.net)
        self.assertFalse(str(yolov_cont) in str(self.orch.client.containers.list(ignore_removed=True)))
        self.assertFalse(str(cntk_cont) in str(self.orch.client.containers.list(ignore_removed=True)))

if __name__ == '__main__':

   orch = organizer.Orchestrator()
   unittest.main()
