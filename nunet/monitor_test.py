import time
import psutil
import datetime
import os
import json
import load_test
from multiprocessing import Process
import stats_test

try:
    time_limit = os.environ['TIME_LIMIT']
except:
    time_limit = None
    print("no env variable time limit")
try:
    requests = os.environ['REQUESTS']
except:
    requests = None
    print("no env variable requests")

try:
    sleep_time = os.environ['SLEEP_TIME']
except:
    sleep_time = None
    print("no env variable sleep_time")

if not time_limit:
    time_limit = 60

if not requests:
    requests = 1

if not sleep_time:
    sleep_time = 0.0001
class SystemMonitor:
    def f(self,filepath,n):
        start_time=int(time.time())
        i=0
        while (True):
            time.sleep(float(n))
            cpu_percent=dict(psutil.virtual_memory()._asdict())['percent']
            with open(filepath, 'a') as f:
                #json.dump(res, json_file, indent=4)
                ind=str(i)
                if i==0:
                    f.write("{"+'"'+ind+'"'':{'+'"cpu_percentage":'+str(psutil.cpu_percent())+','+'"memory_percentage":'+str(dict(psutil.virtual_memory()._asdict())['percent'])+'}')
                else:
                    f.write(',"'+ind+'"'':{'+'"cpu_percentage":'+str(psutil.cpu_percent())+','+'"memory_percentage":'+str(dict(psutil.virtual_memory()._asdict())['percent'])+'}')
            i+=1
            if int(time.time())- start_time > int(time_limit):
                with open("quit.txt","w") as f:
                    f.write("y")
                break            
        st=stats_test.StatsTest()
        st.stats_test(filepath)

    def start_monitor(self):
        now = datetime.datetime.now()
        path="system_monitor/"
        try:
            os.mkdir(path)
        except:          
            pass
        n = sleep_time
        filepath=path+'/'+str(now)+'.json'
  
        Process(target=self.f,args=(filepath,n)).start()
        load_test.run_test(requests)

if __name__ == "__main__":
    monitor = SystemMonitor()
    load_test=load_test.load_test()
    monitor.start_monitor()
