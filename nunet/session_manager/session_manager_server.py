import json
import grpc
import sys
import os
import logging
import argparse
import random
import string
import time
import json
import urllib3
from concurrent import futures
import datetime
from db import ConsumerCredential, TaskExecutions, ConsumerDevice , Tasks, Execution ,Subprocess , RewardTable , ProviderDevice
from db.interface import Database
from fractions import Fraction
from sqlalchemy.ext.declarative import DeclarativeMeta
import base64
import data_manager as dm
import pickle
import session_pb2_grpc as sm_pb2_grpc
import session_pb2 as sm_pb2

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
from organizer import Orchestrator


logging.basicConfig(format="%(asctime)s - [%(levelname)8s] "
                           "- %(name)s - %(message)s", level=logging.INFO)
log = logging.getLogger("session_manager")

ONE_DAY_IN_SECONDS = 60 * 60 * 24

try:
    nunet_port = os.environ['NUNET_PORT']
except:
    nunet_port = None
    print("no env variable NUNET_PORT")

if not nunet_port:
    nunet_port = 50000

class Status:
    OK = 0
    UNAUTHENTICATED = 1
    CANCELLED = 2
    UNKNOWN = 3
    NO_Sufficient_TOKEN =4

class Tag:
    ERROR = "error"
    LOG = "log"
    YOLO_RESULT = "yolo_result"
    YOLO_OUTPUT = "yolo_output"
    CNTK_RESULT = "cntk_result"
    BAD_RESULT = "bad_result"
    CNTK_ERROR = "cntk_error"
    YOLO_STAT = "yolo_stat"
    CNTK_STAT = "cntk_stat"
    NEXT = "next"
    DOG_IM = "dog_im"
    YOLO_SUB_ID = 0
    CNTK_SUB_ID = 1
    TASK_ID ="task_id"
    SAVE_OUTPUT ="save_output"
    YOLO_PROVIDER = "Device 1"
    CNTK_PROVIDER = "Device 2"
    TOP_5="top_5"

def generate_access_token(length=15):
    key = ''.join(random.choice(string.ascii_letters + string.digits)
                  for _ in range(length))
    return key.upper()

class SessionManagerServicer(sm_pb2_grpc.SessionManagerServicer):

    def __init__(self, db_session,  timeout=20):
        self.db = db_session
        self.timeout = timeout
        self.min_token = 20
        self.token_spent_per_process = 15
        self.top_5=''

    def signup(self , request, context):
        cred = self.db.query(ConsumerCredential, email=request.email)
        if not cred:
            if self.db.add(ConsumerCredential, email= request.email, password=self.db.hash_password(request.password)):
                log.info("Credential with email addres '{}' added.".format(request.email))
                return sm_pb2.SignupOutput(status= Status.OK )
            else:
                log.error("Error adding '{}'!".format(request.email))
                return self.set_grpc_context(context,
                                                sm_pb2.SignupOutput(),
                                                "Email addres already in use!",
                                                grpc.StatusCode.ALREADY_EXISTS)
        else:
            return self.set_grpc_context(context,
                                                sm_pb2.SignupOutput(),
                                                "Email addres already in use!",
                                                grpc.StatusCode.ALREADY_EXISTS)




    def login(self, request, context):
        cred = self.db.query(ConsumerCredential, email=request.email, password=request.password)
        if not cred:
            return self.set_grpc_context(context, sm_pb2.LoginOutput(), "User not registered!", grpc.StatusCode.NOT_FOUND)

        if self.db.query_all(Tasks , email = cred.email ):
            first_task_flag = False
        else:
            first_task_flag = True
        # Check if the Device is already registered
        device = self.db.query(ConsumerDevice, device_name=request.device_name, email=request.email)

        # Check if the Device is the active one
        if device and cred.active_device == device.device_name:
            log.warning("Device is already active and logged in.")
            return sm_pb2.LoginOutput(access_token=device.access_token,
                                  bool = first_task_flag)

        access_token = generate_access_token()
        if not device:
            log.info("Registering new Device: '{}'".format(request.device_name))
            self.db.add(ConsumerDevice,device_name=request.device_name,access_token=access_token,email=cred.email)
        elif device.access_token == "":
            log.info("Setting new access_token for: '{}'".format(request.device_name))
            self.db.update(ConsumerDevice, where={"email": cred.email,"device_name": device.device_name},
                                           update={"access_token": access_token})
        else:
            access_token = device.access_token
            log.warning("Device '{}' is already logged in.".format(request.device_name))


        return sm_pb2.LoginOutput(access_token=access_token ,
                                  bool = first_task_flag)

    def logout(self, request, context):
        cred, device, access_token  = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context, sm_pb2.LogoutOutput(), "You have already logged out!", grpc.StatusCode.UNAUTHENTICATED)
        if cred.active_device == request.device_name:
            log.info("The active Device is logging out...")
            self.set_active_device(device.email, "")
        log.info("Updating the ConsumerDevice info...")
        self.db.update(ConsumerDevice,where={"dev_id": device.dev_id},
                                      update={"access_token": ""})
        return sm_pb2.LogoutOutput(status=Status.OK)

    def socialMediaLogin(self, request, context):
        http = urllib3.PoolManager()
        if request.media == 'facebook':
            content = http.request('GET', 'https://graph.facebook.com/me?fields=email,name,picture&access_token='+request.access_token)
        elif request.media == 'google':
            content = http.request('GET', 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+request.access_token)

        access_token = generate_access_token()
        if content.status == 200:
            data = json.loads(content.data.decode('utf-8'))
            #first_name = data['given_name']
            if request.media=='facebook':
                picture = data['picture']['data']['url']
            elif request.media == 'google':
                picture = data['picture']

            cred = self.db.query(ConsumerCredential,  email =data['email'])
            if self.db.query_all(Tasks , email = data['email']):
                first_task_flag = False
            else:
                first_task_flag = True
            if not cred:
                log.info("Registering new user: '{}'".format(data['email']))
                self.db.add(ConsumerCredential,
                            picture= picture,
                            email = data['email'])
                self.db.add(ConsumerDevice,
                            device_name=request.device_name,
                            access_token=access_token,
                            email = data['email'])
                return sm_pb2.LoginOutput(access_token=access_token,
                                  bool = first_task_flag)

            # check if the device is already registered
            device = self.db.query(ConsumerDevice,
                                    device_name=request.device_name,
                                    email = data['email'])
            if device and cred.active_device == device.device_name:
                log.warning("Device is already active and logged in.")
                return sm_pb2.LoginOutput(access_token=device.access_token , bool = first_task_flag)
            if not device:
                log.info("Registering new Device: '{}'".format(request.device_name))
                self.db.add(ConsumerDevice,
                            device_name=request.device_name,
                            access_token=access_token,
                            email = data['email'])
            elif device.access_token == "":
                log.info("Setting new access_token for: '{}'".format(request.device_name))
                self.db.update(ConsumerDevice, where={"email": cred.email,"device_name": device.device_name},
                                            update={"access_token": access_token})

            else:
                access_token = device.access_token
                log.warning("Device '{}' is already logged in.".format(request.device_name))

            return sm_pb2.LoginOutput(access_token=access_token , bool = first_task_flag)
        else:
            return self.set_grpc_context(context, sm_pb2.LoginOutput(), "User not registered!", grpc.StatusCode.UNAUTHENTICATED)



    def userInfo(self, request, context):
        cred, _, access_token = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context, sm_pb2.UserInfoOutput(), "Invalid access!", grpc.StatusCode.UNAUTHENTICATED)
        tasks = self.db.query_all(Tasks , email = cred.email )
        task_run = len(tasks)
        total_token_spent = task_run * self.token_spent_per_process
        total_reward = 0.0
        completed_task = 0
        for task in tasks:
            if self.db.query(TaskExecutions, task_id = task.task_id, order = 1):
                completed_task +=1

        for rewards in tasks:
            if rewards.reward:
                total_reward += rewards.reward


        return sm_pb2.UserInfoOutput(email=cred.email,
                                      picture = cred.picture,
                                      balance = cred.token,
                                      total_token_spent = total_token_spent,
                                      total_reward = total_reward,
                                      task_run = task_run,
                                      completed_task =completed_task

                                      )


    def execute(self, request, context):
        cred, device, access_token  = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context, sm_pb2.ExecutionOutput(), "Invalid access!", grpc.StatusCode.UNAUTHENTICATED)


        if self.min_token  > cred.token:
            return self.set_grpc_context(context, sm_pb2.ExecutionOutput(), "there is no sufficient token", grpc.StatusCode.NO_Sufficient_TOKEN)

        im_data = base64.b64decode(request.base64)
        time = datetime.datetime.now()
        filename = "imdata/{0}_input_im_{1}.jpg".format(cred.email, time)
        with open(filename, 'wb') as f:
            f.write(im_data)
        orc = Orchestrator()
        orc = orc.call(request.base64 , cred.email)


        for tag, lg , breed in orc:
            if tag==Tag.YOLO_RESULT:
                #dm.updateToken(self.db, cred, 10 )
                yolo_execution_id , task_id= dm.addTask(self.db, cred ,  lg ,Tag.YOLO_SUB_ID)
                yield sm_pb2.ExecutionOutput(log_info = lg, tag= tag)
                yield sm_pb2.ExecutionOutput(log_info = str(task_id), tag= Tag.TASK_ID)

            elif tag==Tag.BAD_RESULT:
                dm.updateToken(self.db , cred, - 10)
                yolo_execution_id , task_id= dm.addTask(self.db, cred , lg , Tag.YOLO_SUB_ID)
                yield sm_pb2.ExecutionOutput(log_info = lg, tag= tag)
                yield sm_pb2.ExecutionOutput(log_info = str(task_id), tag= Tag.TASK_ID)
            elif tag==Tag.ERROR:
                yolo_execution_id, task_id  = dm.addTask(self.db, cred ,  lg, Tag.YOLO_SUB_ID )
                yield sm_pb2.ExecutionOutput(log_info = lg, tag = tag)
                yield sm_pb2.ExecutionOutput(log_info = str(task_id), tag= Tag.TASK_ID)
            elif tag==Tag.CNTK_ERROR:
                cntk_execution_id = dm.addExecution(self.db , lg, Tag.CNTK_SUB_ID , task_id )
                yield sm_pb2.ExecutionOutput(log_info = lg, tag = tag)
            elif tag==Tag.DOG_IM:
                print("dog im")
            elif tag==Tag.SAVE_OUTPUT:
                self.db.update(Execution, where = { "execution_id" : yolo_execution_id},
                                          update ={"image_output" : lg}  )

            elif tag==Tag.CNTK_RESULT:
                reward = self.db.query(RewardTable, breed_name = breed)
                if not reward:
                    reward = 20
                else:
                    reward = reward.reward
                cntk_execution_id = dm.addExecution(self.db, lg, Tag.CNTK_SUB_ID , task_id  )
                self.db.update(Tasks, where={"task_id": task_id},
                                      update={"reward": reward })
                dm.updateToken(self.db , cred, reward - self.token_spent_per_process)
                yield sm_pb2.ExecutionOutput(log_info = lg, tag= tag)

            elif tag==Tag.YOLO_OUTPUT:

                yield sm_pb2.ExecutionOutput(log_info = lg,  tag= tag)

            elif tag==Tag.TOP_5:
                self.db.update(Execution, where={"execution_id": cntk_execution_id},
                                          update={"json_result": lg })

            elif tag==Tag.YOLO_STAT or tag==Tag.CNTK_STAT:
                log.info("Registering new subprocess:")
                if tag==Tag.YOLO_STAT:
                    dm.updateExecution(self.db, yolo_execution_id,lg, filename)
                    dm.updateProviderDeviceData(self.db,Tag.YOLO_PROVIDER , lg )
                else:
                    dm.updateExecution(self.db, cntk_execution_id,lg)
                    dm.updateProviderDeviceData(self.db,Tag.CNTK_PROVIDER , lg )
                stat= json.dumps(lg)
                yield sm_pb2.ExecutionOutput(log_info =stat, tag= tag)

            else:
                yield sm_pb2.ExecutionOutput(log_info = lg,tag= tag)


    def previousTasks(self, request, context):
        cred, _, access_token = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context, sm_pb2.PreviousTasksOutput(), "Invalid access!",   grpc.StatusCode.UNAUTHENTICATED)
        size = request.offset + request.size
        tasks = self.db.query_by_limit(Tasks, request.offset, size ,Tasks.date, email = cred.email)
        response= sm_pb2.PreviousTasksOutput()
        for task in tasks:
            task_execution = self.db.query(TaskExecutions , task_id=task.task_id ,order= 0 )
            execution = self.db.query(Execution , execution_id=task_execution.execution_id ,subprocess_id = 0 )
            with open(execution.input_image, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
            dog_im = encoded_string
            response.previousTasks.add(task_id = task.task_id,
                                       index = task.index,
                                       base64=dog_im,
                                       result=execution.result,
                                       seconds= int((task.date - datetime.datetime(1970,1,1)).total_seconds()) )


        return  response


    def processInfo(self,  request, context):
        _, _, access_token = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context, sm_pb2.ProcessInfoOutput(),"Invalid access!", grpc.StatusCode.UNAUTHENTICATED)

        yolo_task_execution = self.db.query(TaskExecutions ,task_id = request.task_id ,order =Tag.YOLO_SUB_ID )
        if not yolo_task_execution:
            log.error("there is no task_execution  whith this task Id")
            return self.set_grpc_context(context,
                                         sm_pb2.ProcessInfoOutput(),
                                         "There is no task_execution with this task id.",
                                         grpc.StatusCode.NOT_FOUND)
        yolo_execution_id = yolo_task_execution.execution_id
        yolo_execution = self.db.query(Execution, execution_id = yolo_execution_id)
        cntk_task_execution = self.db.query(TaskExecutions, task_id = request.task_id , order= Tag.CNTK_SUB_ID)
        if not cntk_task_execution:
            network_tx = yolo_execution.network_tx
            network_rx = yolo_execution.network_rx
            time_taken = yolo_execution.time_taken
            cpu_usage = yolo_execution.cpu_usage
            memory_usage = yolo_execution.memory_usage
            total_memory=yolo_execution.total_memory
            result = yolo_execution.result
            json_result=""
        else:
            cntk_execution_id = cntk_task_execution.execution_id
            cntk_execution = self.db.query(Execution, execution_id = cntk_execution_id)
            network_tx = (yolo_execution.network_tx + cntk_execution.time_taken)
            network_rx = (yolo_execution.network_rx + cntk_execution.time_taken)
            time_taken = (yolo_execution.time_taken + cntk_execution.time_taken)
            cpu_usage = (yolo_execution.cpu_usage + cntk_execution.cpu_usage)
            memory_usage = (yolo_execution.memory_usage + cntk_execution.memory_usage)
            total_memory = (yolo_execution.total_memory + cntk_execution.total_memory)
            result = cntk_execution.result
            json_result=cntk_execution.json_result
        if yolo_execution.image_output:
            with open(yolo_execution.image_output, "rb") as image_file:
                    encoded_string = base64.b64encode(image_file.read())
            image_out = encoded_string
        else:
            image_out = ""

        reward = self.db.query(Tasks, task_id = request.task_id)
        reward = reward.reward




        return sm_pb2.ProcessInfoOutput( result = result,
                                            token_spent = self.token_spent_per_process,
                                            token_reward = reward,
                                            time_taken = time_taken,
                                            memory_usage = memory_usage,
                                            json_result=json_result,
                                            network_tx= network_tx,
                                            network_rx = network_rx,
                                            cpu_usage =  cpu_usage,
                                            image_output = image_out,
                                            total_memory=total_memory)

    def provider(self, request, context):
        cred, _, access_token = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context,
                                         sm_pb2.PreviousTasksOutput(),
                                         "Invalid access!",
                                         grpc.StatusCode.UNAUTHENTICATED)

        devices = self.db.query_all(ProviderDevice)
        response= sm_pb2.ProviderOutput()
        for dev in devices:
            response.device.add(device_name = dev.device_name,
                                process_completed=dev.process_completed,
                                token_earned= dev.token_earned )


        return  response


    def providerDevice(self, request, context):
        cred, _, access_token = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context,
                                         sm_pb2.UserInfoOutput(),
                                         "Invalid access!",
                                         grpc.StatusCode.UNAUTHENTICATED)
        device = self.db.query(ProviderDevice , device_name = request.device_name )

        return sm_pb2.ProviderDeviceOutput(cpu_limit=device.cpu_limit,
                                      cpu_used = device.cpu_used,
                                      memory_limit = device.memory_limit,
                                      memory_used = device.memory_used,
                                      net_limit = device.net_limit,
                                      net_used = device.net_used,
                                      max_up_time= device.up_time_limit,
                                      used_up_time= device.up_time_used,
                                      cpu_price = device.cpu_price,
                                      ram_price = device.ram_price,
                                      net_price = device.net_price

                                      )

    def rewardTable(self, request, context):
        cred, device, access_token  = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context,
                                         sm_pb2.RewardTableOutput(), "Invalid access!", grpc.StatusCode.UNAUTHENTICATED)

        size = request.offset + request.size
        q_rewards = self.db.query_by_limit(RewardTable, request.offset, size ,RewardTable.reward)

        response= sm_pb2.RewardTableOutput()
        for r in q_rewards:
            with open(r.breed_image, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
            breed_image = encoded_string
            response.rewards.add(base64=breed_image,
                                breed_name=r.breed_name,
                                reward= r.reward,
                                discovered_by_comunity = 5)


        return  response




    def updateUserToken(self, request, context):
        cred, _, access_token = self.validate_access(context)
        if not access_token:
            return self.set_grpc_context(context,
                                         sm_pb2.UserInfoOutput(),
                                         "Invalid access!",
                                         grpc.StatusCode.UNAUTHENTICATED)
        if cred.token < self.min_token:
            dm.updateToken(self.db , cred, 50)
            credbalance = self.db.query(ConsumerCredential, email=cred.email)
            return sm_pb2.UpdateTokenOutput(
                updated_balance = credbalance.token
            )
        else:
            return self.set_grpc_context(context,
                                         sm_pb2.UserInfoOutput(),
                                         "You have sufficient token to use the service",
                                         grpc.StatusCode.CANCELLED)







    def validate_access(self, context):
        access_token = self.get_access_token(context.invocation_metadata())
        if not access_token:
            log.error("No access token!")
            return None, None, None
        device = self.db.query(ConsumerDevice, access_token=access_token)

        if not device:
            log.error("Device not registered!")
            return None, None, None
        cred = self.db.query(ConsumerCredential, email=device.email)
        if not cred:
            log.error("User not registered!")
            return None, None, None

        return cred, device, access_token

    # Set the active_device column of ConsumerCredential with the device_name
    def set_active_device(self, email, device_name):
        log.info("Setting {}.active_device to '{}'".format(email,
                                                           device_name))
        return self.db.update(ConsumerCredential,
                              where={"email": email},
                              update={"active_device": device_name})


    @staticmethod
    def set_grpc_context(context, message_type, msg, code=None):
        log.warning(msg)
        context.set_details(msg)
        if code:
            context.set_code(code)
        return message_type
    # Checks if the incoming request is valid

    @staticmethod
    def get_access_token(metadata):
        for key, value in metadata:
            if key == "access_token" and value:
                return value
        return None
    @staticmethod
    def object_as_dict(obj):
        return {c.key: getattr(obj, c.key)
                for c in inspect(obj).mapper.column_attrs}



class SessionManagerServer:
    def __init__(self,
                 db_file="sessions.db",
                 db_create=False,
                 port=nunet_port,
                ):

        self.db_file = db_file
        self.db_create = db_create
        self.db = Database(db_file=db_file, db_create=db_create)
        self.port = port
        self.server = None
        self.timeout = 30
    def start_server(self):
        hundred_MB = (1024 ** 2) * 100   # max grpc message size

        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=20),
            options=[
            ('grpc.max_receive_message_length', hundred_MB)
        ])

        sm_pb2_grpc.add_SessionManagerServicer_to_server(
            SessionManagerServicer(db_session=self.db,
                                   timeout=self.timeout), self.server )
        self.server.add_insecure_port("[::]:{}".format(self.port))
        log.info("Starting SessionManagerServer at localhost:{}".format(
            self.port))
        self.server.start()

    def stop_server(self):
        self.server.stop(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--db-file",
                        "-db",
                        dest="db_file",
                        default="sessions.db",
                        help="DB file name")
    parser.add_argument("--db-create",
                        action="store_true",
                        dest="db_create",
                        default=False,
                        help="Force DB creating")
    parser.add_argument("--port",
                        "-p",
                        type=int,
                        default=nunet_port,
                        help="Session manager server port")

    args = parser.parse_args()

    server = SessionManagerServer(db_file=args.db_file,
                                  db_create=args.db_create,
                                  port=args.port,

                                  )
    server.start_server()

    try:
        while True:
            time.sleep(ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop_server()
