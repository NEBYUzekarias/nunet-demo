import logging
import grpc
import session_manager_client as sm
from datetime import datetime
import json
import os

class Session:

    def __init__(self):
        self.stub = sm.get_stub()
        self.email = "test2@test.com"
        self.password="test2@test.com"
        self.device_name="samsung"
        self.token=""
        self.img=["artifacts/image1/img1.jpg","artifacts/image1/img2.jpg","artifacts/image1/img3.jpg","artifacts/image1/img4.jpg","artifacts/image1/img5.jpg","artifacts/image1/img6.jpg","artifacts/image1/img7.jpg","artifacts/image1/img8.jpg","artifacts/image1/img9.jpg","artifacts/image1/img10.jpg"]

    def signup(self):
        try:
             sm.signup(self.stub, self.email, self.password)
        except:
             logging.info("You have already registered")

    def login(self):
        tkn=sm.login(self.stub, self.email, self.password,self.device_name)
        self.token=tkn

    def execute(self):
        yolo,cntk=sm.Execute(self.stub, self.token, "bulldog.jpg")
        print("******************************")
        logging.info("**************************")
        print(yolo)
        print(cntk)
    
    def execute_resource_usage(self):
        average_yolo=[0,0,0,0,0,0,0]
        average_cntk=[0,0,0,0,0,0,0]
        sd_yolo=[0,0,0,0,0,0,0]
        sd_cntk=[0,0,0,0,0,0,0]
        open("resource_usage.txt", 'w')
        conts=["yolov","cntk"]
        tags=["max_memory","total_cpu","total_memory","time_taken","net_tx","net_rx"]
        #0:mem_usage,#1:cpu,#2total_mem,#3time_taken,#4nettx,#5netrx

        xi_yolo=[]
        xi_cntk=[]
        image_sizes=[]

        for i in range(10):

            image_sizes.append(os.stat(self.img[i]).st_size)
            yolo,cntk=sm.Execute(self.stub, self.token, self.img[i])
            xi_yolo.append(json.loads(yolo))
            xi_cntk.append(json.loads(cntk))
            #average_yolo.append(json.loads(yolo)['memory_usage'])
            average_yolo[0]+=json.loads(yolo)['memory_usage'] #total_memory_max
            average_yolo[1]+=json.loads(yolo)['cpu_usage'] #atotal_max_cpu
            average_yolo[2]+=json.loads(yolo)['total_memory'] #total_total_memory
            average_yolo[3]+=json.loads(yolo)['time_taken'] #total_time_taken
            average_yolo[4]+=json.loads(yolo)['net_tx']#total_tx
            average_yolo[5]+=json.loads(yolo)['net_rx']#total_rx

            average_cntk[0]+=json.loads(cntk)['memory_usage'] #total_memory_max
            average_cntk[1]+=json.loads(cntk)['cpu_usage'] #atotal_max_cpu
            average_cntk[2]+=json.loads(cntk)['total_memory'] #total_total_memory
            average_cntk[3]+=json.loads(cntk)['time_taken'] #total_time_taken
            average_cntk[4]+=json.loads(cntk)['net_tx']#total_tx
            average_cntk[5]+=json.loads(cntk)['net_rx']#total_rx

        #mean
        for i in range(len(average_cntk)):
            average_yolo[i]/=len(xi_yolo)
            average_cntk[i]/=len(xi_cntk)
        i=0

        with open("resource_usage.txt", 'a') as f:
            f.write("[\n")

        for x_yolo in xi_yolo:
            sd_yolo[0]+=(((x_yolo['memory_usage']-average_yolo[0])**2)/len(xi_yolo))**0.5
            sd_yolo[1]+=(((x_yolo['cpu_usage']-average_yolo[1])**2)/len(xi_yolo))**0.5
            sd_yolo[2]+=(((x_yolo['total_memory']-average_yolo[2])**2)/len(xi_yolo))**0.5
            sd_yolo[3]+=(((x_yolo['time_taken']-average_yolo[3])**2)/len(xi_yolo))**0.5
            sd_yolo[4]+=(((x_yolo['net_tx']-average_yolo[4])**2)/len(xi_yolo))**0.5
            sd_yolo[5]+=(((x_yolo['net_rx']-average_yolo[5])**2)/len(xi_yolo))**0.5
            image_size=str(image_sizes[i])
            ind=str(i)+"_yolo"
            with open("resource_usage.txt", 'a') as f:             
                if i==0:
                    f.write("{"+'"process_name":"'+ind+'",'+'"image_size":'+image_size+','+'"max_memory":'+str(x_yolo['memory_usage'])+','+'"total_cpu":'+str(x_yolo['cpu_usage'])+','+'"total_memory":'+str(x_yolo['total_memory'])+','+'"time_taken":'+str(x_yolo['time_taken'])+','+'"net_tx":'+str(x_yolo['net_tx'])+','+'"net_rx":'+str(x_yolo['net_rx'])+'},\n')
                else:
                    f.write("{"+'"process_name":"'+ind+'",'+'"image_size":'+image_size+','+'"max_memory":'+str(x_yolo['memory_usage'])+','+'"total_cpu":'+str(x_yolo['cpu_usage'])+','+'"total_memory":'+str(x_yolo['total_memory'])+','+'"time_taken":'+str(x_yolo['time_taken'])+','+'"net_tx":'+str(x_yolo['net_tx'])+','+'"net_rx":'+str(x_yolo['net_rx'])+'},\n')

            i+=1
        i=0
        for x_cntk in xi_cntk:
            sd_cntk[0]+=(((x_cntk['memory_usage']-average_cntk[0])**2)/len(xi_yolo))**0.5
            sd_cntk[1]+=(((x_cntk['cpu_usage']-average_cntk[1])**2)/len(xi_yolo))**0.5
            sd_cntk[2]+=(((x_cntk['total_memory']-average_cntk[2])**2)/len(xi_yolo))**0.5
            sd_cntk[3]+=(((x_cntk['time_taken']-average_cntk[3])**2)/len(xi_yolo))**0.5
            sd_cntk[4]+=(((x_cntk['net_tx']-average_cntk[4])**2)/len(xi_yolo))**0.5
            sd_cntk[5]+=(((x_cntk['net_rx']-average_cntk[5])**2)/len(xi_yolo))**0.5
            ind=str(i)+"_cntk"
            image_size=str(image_sizes[i])
            with open("resource_usage.txt", 'a') as f:             
                f.write("{"+'"process_name":"'+ind+'",'+'"image_size":'+image_size+','+'"max_memory":'+str(x_cntk['memory_usage'])+','+'"total_cpu":'+str(x_cntk['cpu_usage'])+','+'"total_memory":'+str(x_cntk['total_memory'])+','+'"time_taken":'+str(x_cntk['time_taken'])+','+'"net_tx":'+str(x_cntk['net_tx'])+','+'"net_rx":'+str(x_cntk['net_rx'])+'},\n')
            i+=1
        for cont in conts:
            for i in range(len(tags)):
                if cont=="yolov":
                    print("average:"+str(cont)+"_"+str(tags[i])+":"+str(average_yolo[i]))
                    print("standard deviation:"+str(cont)+"_"+str(tags[i])+":"+str(sd_yolo[i]))
                else:
                    print("average"+str(cont)+"_"+str(tags[i])+":"+str(average_cntk[i]))
                    print("standard deviation"+str(cont)+"_"+str(tags[i])+":"+str(sd_cntk[i]))
        
        with open("resource_usage.txt", 'a') as f:
            f.write("{"+'"average_yolo"'+':{'+'"max_memory":'+str(average_yolo[0])+','+'"total_cpu":'+str(average_yolo[1])+','+'"total_memory":'+str(average_yolo[2])+','+'"time_taken":'+str(average_yolo[3])+','+'"net_tx":'+str(average_yolo[4])+','+'"net_rx":'+str(average_yolo[5])+'},\n')                
            f.write('"average_cntk"'+':{'+'"max_memory":'+str(average_cntk[0])+','+'"total_cpu":'+str(average_cntk[1])+','+'"total_memory":'+str(average_cntk[2])+','+'"time_taken":'+str(average_cntk[3])+','+'"net_tx":'+str(average_cntk[4])+','+'"net_rx":'+str(average_cntk[5])+'},\n')
            f.write('"standard_deviation_yolo"'+':{'+'"max_memory":'+str(sd_yolo[0])+','+'"total_cpu":'+str(sd_yolo[1])+','+'"total_memory":'+str(sd_yolo[2])+','+'"time_taken":'+str(sd_yolo[3])+','+'"net_tx":'+str(sd_yolo[4])+','+'"net_rx":'+str(sd_yolo[5])+'},\n')                
            f.write('"standard_deviation_cntk"'+':{'+'"max_memory":'+str(sd_cntk[0])+','+'"total_cpu":'+str(sd_cntk[1])+','+'"total_memory":'+str(sd_cntk[2])+','+'"time_taken":'+str(sd_cntk[3])+','+'"net_tx":'+str(sd_cntk[4])+','+'"net_rx":'+str(sd_cntk[5])+'}}\n]')
  
        with open("resource_usage.txt", 'r') as f:
           print(f.read())

    def previoustasks(self):
        print("result:",str(sm.previousTasks(self.stub, self.token, 0,2)).split("\"")[3])
        print("date:",datetime.fromtimestamp(int(str(sm.previousTasks(self.stub, self.token, 0,2)).split("\"")[4][12:22])))
        print("task number:",str(sm.previousTasks(self.stub, self.token, 0,2)).split("\"")[4][35:37])

    def rewardtable(self):
        print("breed name:",str(sm.RewardTable(self.stub,self.token)).split("\"")[1])
        print(str(sm.RewardTable(self.stub,self.token)).split("\"")[4][0:43])

    def logout(self):
        try:
           sm.logout(self.stub,self.device_name,self.password)
        except:
            logging.info("You have already logged out")

if __name__ == '__main__':
   sess=Session()
   sess.signup()
   sess.login()
   sess.execute_resource_usage()
   #sess.previoustasks()
   #sess.rewardtable()
   sess.logout()
