import docker
import grpc

import os
import base64
import object_detection_pb2_grpc
import object_detection_pb2

import image_recon_pb2_grpc
import image_recon_pb2

import logging
import random
import datetime
import time

import json
import ast
import cv2
import io
from io import BytesIO
from PIL import Image
import requests
import numpy as np
from io import BytesIO
import multiprocessing
from multiprocessing import Process,current_process

from random import randint
# name of nunet container
from constants import CONTAINER_NAME

class Tag:
    ERROR = "error"
    LOG = "log"
    YOLO_RESULT = "yolo_result"
    YOLO_OUTPUT = "yolo_output"
    CNTK_RESULT = "cntk_result"
    BAD_RESULT = "bad_result"
    CNTK_ERROR = "cntk_error"
    YOLO_STAT = "yolo_stat"
    CNTK_STAT = "cntk_stat"
    NEXT = "next"
    DOG_IM = "dog_im"
    SAVE_OUTPUT ="save_output"
    TOP_5="top_5"


class Orchestrator:

    def __init__(self):
        self.image_repos = {"yolov3-object-detection":"https://raw.githubusercontent.com/iCogLabs/dnn-model-services/master/services/yolov3-object-detection/Dockerfile",
                            "cntk-image-recon":"https://raw.githubusercontent.com/iCogLabs/dnn-model-services/master/services/cntk-image-recon/Dockerfile"}
        self.client = docker.from_env(timeout=10000)
        self.dapi = docker.APIClient()
        self.dog_im  =""


    def build(self, image_name):
        """
        This builds images specified using images.build option.

        :param image_name: the image from the self.image_repos that is going to be built.
        """

        try:
            img_ret, json_respone = self.client.images.build(path=self.image_repos[image_name],tag=image_name)
            logging.info("Building Docker image ", image_name)
            yield Tag.LOG , "Building Docker image "+image_name+ "..." , None
            for i in json_respone:
                logging.info(i)
            logging.info("Docker Image " + image_name + " Build...")
            yield Tag.LOG ,(f"Docker Image {image_name } Build") , None
        except TypeError as e:
            logging.error("Docker Build Error: Neither path nor fileobj is specified. ", e)
            yield Tag.ERROR ,"Docker Build Error: Neither path nor fileobj is specified" , None
        except docker.errors.BuildError as ex:
            logging.error("Building Docker image ", image_name, "failed. ", ex)
            yield Tag.ERROR, (f"Building Docker image {image_name} failed. ") , None
        except docker.errors.APIError as exc:
            if exc.response.status_code==500:
                logging.error("{path} is not correct url".format(path=self.image_repos[image_name]))
                yield Tag.ERROR, "{path} is not correct url".format(path=self.image_repos[image_name]) , None
            elif exc.response.status_code==400:
                logging.error(image_name, " is not found or you don't have pull access.".format(image=image_name))
                yield Tag.ERROR, (image_name, " is not found or you don't have pull access.".format(image=image_name)) , None
            else:
                logging.error(exc.response.status_code)
                yield Tag.ERROR , "error" , None

        # we need to capture the various issues. And inform the user about that. The output above is quite not
        # insightful.

    def create(self, id, image_name, argument):
        """
        This creates the container using the image name.

        The listing of the arguments is going to be tough. We need to make it specific that it would be good.

        :param argument: The startup arguments for the image.
        :param image_name: The image that is going to create a container
        """
        container = " "
        ip = " "

        try:
            container = self.client.containers.run(image_name, detach=True, command= argument)#, network = net, command= argument, detach=True)
            netw = self.client.networks.get(id)
            netw.connect(container=container)
            logging.info("Network ID " + netw.id)
            logging.info("Container " + container.name + " running")
            dict = self.dapi.inspect_container(container.name).get('NetworkSettings')
            nw = dict.get('Networks')
            fin = list(nw.values())[1]
            logging.info("IP Address of Container: " + container.name + fin.get("IPAddress"))
            ip = fin.get("IPAddress")

        except docker.errors.ContainerError as e:
            log = container.exec_run('ls',
                             stderr=True,
                             stdout=True)
            logging.error("exits with exit code {exit_code}".format(exit_code=log[0]))

        except docker.errors.ImageNotFound:
            logging.error("{image_name} Image is not found".format(image_name=image_name))

        except docker.errors.APIError as ed:
             logging.error(ed)

        #TODO find means to store this value.
        #f.close()
        return container, ip


    def call_yolo_cont(self, ip, container, port, img , username,yolo_file ):
        start_time=int(time.time())
        hundred_MB = (1024 ** 2) * 100   # max grpc message size
        channel = grpc.insecure_channel(str(ip)+":"+str(port), options=[
            ('grpc.max_send_message_length', hundred_MB),
            ('grpc.max_receive_message_length', hundred_MB)
        ])
        stub = object_detection_pb2_grpc.DetectStub(channel)
        bool = False
        grpc_method = "detect"
        model = "yolov3"
        confidence = "0.7"
        current_time = datetime.datetime.now()
        image_name ="imdata/{0}_{1}.jpg".format(username, current_time)
        request = object_detection_pb2.Input(model=model, confidence=confidence, img_path=img)
        logging.info("Calling Object Detection grpc service ...")
        logging.info(str(ip)+":"+str(port))
        yield None,Tag.LOG, "Calling Object Detection grpc service ..."
        try:
            response = stub.detect(request)
            net = self.client.networks.get(CONTAINER_NAME)
            self.delete(container, net)
            if "16" in response.class_ids: #16 is the class ID for dog. TODO: Handling this the better way
                logging.info("Dog is detected in the picture")
                yield None, Tag.YOLO_RESULT, "Dog is detected in the picture."
                yield None, Tag.YOLO_OUTPUT , response.img_base64

                imgdata = Image.open(io.BytesIO(base64.b64decode(img)))
                im = cv2.cvtColor(np.array(imgdata), cv2.COLOR_RGB2BGR)
                st = ast.literal_eval(response.boxes)
                cnt = 0
                dog = 0
                for i in st:
                    res=[]
                    for j in i:
                        res.append(round(abs(j)))
                    if ast.literal_eval(response.class_ids)[cnt]==16:
                        x = res[0]
                        y = res[1]
                        w = res[2]
                        h = res[3]
                        ROI = im[y:y+h, x:x+w]
                        if dog==0:
                            cv2.rectangle(im,(x,y),(x+w,x+h),(0,255,0),3)
                            cv2.imwrite(image_name,im)
                            yield None, Tag.SAVE_OUTPUT , image_name
                            with open(image_name, "rb") as image_file:
                               encoded_string = base64.b64encode(image_file.read())
                            self.dog_im = encoded_string
                        dog += 1
                    cnt += 1

                bool = True#return True
            else:
                logging.info("Dog is not detected in the provided picture")
                yield None, Tag.BAD_RESULT, "Dog is not detected in the provided picture."
                bool = False
            logging.info("Successfully called Object Detection grpc service ...")
            yield None,Tag.LOG, "Successfully called Object Detection grpc service ..."

        except grpc.RpcError as e:
            if e.code() == grpc.StatusCode.INVALID_ARGUMENT:
                logging.error("Invalid argument in grpc call to Object detection")
                yield None, Tag.ERROR, "Invalid argument in grpc call to Object detection"
                st, metadata = get_status_metadata(e)
                logging.error(st, ": ", metadata)
            else:
                logging.error('Detect method failed with {0}: {1}'.format(e.code(), e.details()))
                yield None,Tag.ERROR, 'Detect method failed with {0}: {1}'.format(e.code(), e.details())
                #TODO caught exceptions to GRPC calls via StatusCode
        delta_time=float(format((time.time()-start_time),'.2f'))
        stat=self.get_resource_usage(yolo_file,delta_time)
        yield None ,Tag.YOLO_STAT , stat
        yield bool , Tag.DOG_IM , self.dog_im

    def call_cntk_cont(self, ip, container, port, img,cntk_file):
        # open a gRPC channel
        # open a gRPC channel
        start_time=time.time()
        hundred_MB = (1024 ** 2) * 100
        channel2 = grpc.insecure_channel(str(ip)+":"+str(port), options=[
            ('grpc.max_send_message_length', hundred_MB),
            ('grpc.max_receive_message_length', hundred_MB)
        ])
        stub2 = image_recon_pb2_grpc.RecognizerStub(channel2)
        grpc_method = "dogs"
        model = "ResNet152"
        request2 = image_recon_pb2.Input(model=model, img_path=img)

        if grpc_method == "dogs":
            try:
                logging.info("Calling CNTK Image Recognition grpc service ...")
                yield Tag.LOG,"Calling CNTK Image Recognition grpc service ..." , None
                response2 = stub2.dogs(request2)
                net = self.client.networks.get(CONTAINER_NAME)
                self.delete(container, net)
                logging.info("Successfully called CNTK Image Recognition grpc service")
                yield Tag.LOG, "Successfully called CNTK Image Recognition grpc service" , None
                res = ast.literal_eval(response2.top_5)[1].split(': ')

                logging.info("Dog Classification is " + res[1] + " with " + res[0] + " confidence level")
                yield Tag.CNTK_RESULT, f"Dog breed is {res[1]} with {res[0]} confidence level." , res[1]
                yield Tag.TOP_5,response2.top_5,None
            except grpc.RpcError as e:
                if e.code() == grpc.StatusCode.INVALID_ARGUMENT:
                    logging.error("Invalid argument in grpc call to CNTK Image Recognition")
                    yield Tag.CNTK_ERROR, "Invalid argument in grpc call to CNTK Image Recognition" , None
                    st, metadata = get_status_metadata(e)
                    logging.error("LOG: ", st, ": ", metadata)
                else:
                    logging.error(e)
                    yield Tag.CNTK_ERROR, "Invalid argument in grpc call to CNTK Image Recognition" , None
                    #TODO caught exceptions to GRPC calls via StatusCode

        else:
            logging.info("CNTK Image Recognition grpc service called with Invalid Method")
            yield Tag.ERROR , "CNTK Image Recognition grpc service called with Invalid Method" , None
        delta_time=float(format((time.time()-start_time),'.2f'))

       # time.sleep(2)
        stat=self.get_resource_usage(cntk_file,delta_time)
        yield Tag.CNTK_STAT, stat , None

    def call(self, img_path, username):
        net = self.client.networks.get(CONTAINER_NAME)
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        logging.basicConfig(filename='log/nunet_' + username + '.log', filemode='a', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
        console = logging.StreamHandler()
        logging.getLogger("").addHandler(console)
        self.build("yolov3-object-detection")
        self.build("cntk-image-recon")
        yolo_cont = ""
        yolo_file=username+"yolo"+str(int(time.time()))+str(randint(100,10000))
        cntk_file=username+"cntk"+str(int(time.time()))+str(randint(100,10000))
        open(yolo_file+'.txt', 'w')
        open(cntk_file+'.txt', 'w')
        if img_path:
            # Create unique Network for specific call
            yolo_arg = ["python3", "-m", "service.object_detection_service", "--grpc-port", "8889"]
            yolo_cont, yolo_ip = self.create(net.id, "yolov3-object-detection", yolo_arg)
            process1=Process(target=self.stream_stat_cont, args=(yolo_cont,yolo_file,))
            process1.start()
            yield Tag.LOG, "Create yolov3 object detection ...", None
            if type(yolo_cont) != str:  # if there is no error in the container
                cntk_arg = ["/root/anaconda3/envs/cntk-py35/bin/python", "-m", "service.image_recon_service",
                            "--grpc-port", "8890"]
                cntk_cont, cntk_ip = self.create(net.id, "cntk-image-recon", cntk_arg)
                process2=Process(target=self.stream_stat_cont, args=(cntk_cont,cntk_file,))
                process2.start()
                if type(cntk_cont) != str:  # if there is no error in the container
                        bool = self.call_yolo_cont(yolo_ip, yolo_cont, 8889, img_path, username,yolo_file)
                        for booll, tag, log in bool:
                            yield tag, log, None
                            if booll:  # if dog picture returned from yolo
                                yield Tag.NEXT, "CNTK", None
                                yield Tag.LOG, "Create cntk-image-recon ...", None
                                cntk_im = img_path
                                call_cntk = self.call_cntk_cont(cntk_ip, cntk_cont, 8890, cntk_im,cntk_file)
                                for tag, log, res in call_cntk:
                                    yield tag, log, res
                yield Tag.LOG, "Containers deleted", None
        elif type(yolo_cont) == str:
            logging.error("Port already open")

    def cont_stat(self, log,cont,time_taken):
       logs=log
       previousCPU = logs['precpu_stats']['cpu_usage']['total_usage']
       cpuDelta = logs['cpu_stats']['cpu_usage']['total_usage'] - previousCPU #calculate the change for the cpu usage of the container in between readings
       previousSystem = logs['precpu_stats']['system_cpu_usage']
       systemDelta = logs['cpu_stats']['system_cpu_usage'] - previousSystem
       cpu_percent = 0
       if systemDelta > 0.0 and cpuDelta > 0.0:
           #calculate the change for the entire system between readings
           cpu_percent = ((cpuDelta / systemDelta) * len(logs['cpu_stats']['cpu_usage']['percpu_usage']) * 100) / multiprocessing.cpu_count()
           logging.info("CPU percentage of {container} :{0:.2f}%".format(cpu_percent,container=cont.image))
       else:
           logging.info("CPU percentage: "+ cpu_percent)
       memory_usage=(logs['memory_stats']['usage'])/10**6 #converting to MiB
       memory_limit=(logs['memory_stats']['limit'])/10**9 #converting to GiB
       memory_perc=memory_usage*100/(memory_limit*10**3)
       rx=logs['networks']['eth0']['rx_bytes']
       tx=logs['networks']['eth0']['tx_bytes']
       net_rx = rx/1024
       net_tx = tx/1024
       logging.info("Time Taken: {tm} seconds".format(tm=time_taken))
       logging.info("Memory Percentage: {mem_perc:.2f} %".format(mem_perc=memory_perc))
       logging.info("NET I/O: {rx} Kib / {tx}Kib".format(rx=rx/1024,tx=tx/1024))
       stat = {
           "memory_limit": memory_limit,
           "time_taken": time_taken,
           "memory_usage": memory_usage, # is not memory percentage
           "net_rx": net_rx,
           "net_tx": net_tx,
           "cpu_usage":((cpuDelta)/100)/10**6  # is not cpu percentage
       }
       return  stat

    def delete(self, container, net):
        """
        Stop and Delete a container and associated network
        :param container:
        """
        try:
            container.stop()
            container.remove()
            logging.info("Container " + container.name + " deleted")
            #self.client.networks.prune()

        except docker.errors.APIError as api_error:
            logging.error("Container " + container.name + " can't be deleted")
            #if is_not_found(api_error):
            #    return
            #raise
        
    def stream_stat_cont(self,cont,yolo_file):
        log=cont.stats(decode=True,stream=True)
        total_log=[]
        i=0
        for logs in log:

            total_log.append(logs)

        for log in total_log:
            try:
                if i>1:    
                        res=self.cont_stat(log,cont,0)
                        if i==2:
                            with open(yolo_file+'.txt', 'a') as json_file:
                                json_file.write('{'+"'"+str(i)+"'"+":"+str(res))
                        else:
                            with open(yolo_file+'.txt', 'a') as json_file:
                                json_file.write(","+"'"+str(i)+"'"+":"+str(res))
            except:
                pass

            i+=1
    def get_resource_usage(self,res_file,delta_time):
        with open(res_file+'.txt', 'r') as reader:
            contents=reader.read()
        data=contents.replace("'",'"')+"}"  
        cpu_total=0
        max_memory=0
        net_rx=0
        net_tx=0
        time_taken=0
        total_memory=0
        memory_limit=0
        json_data=json.loads(data)
        index_mem=0
        for i in range(len(json_data)):
            if i>1:
                index=str(i)
                cpu_total+=json_data[index]['cpu_usage']
                if json_data[index]['memory_usage']>max_memory:
                    max_memory=json_data[index]['memory_usage']
                    index_mem=index
                total_memory+=json_data[index]['memory_usage']    
        try:
            net_rx=json_data[index_mem]['net_rx']
            net_tx=json_data[index_mem]['net_tx']
            memory_limit=json_data[index_mem]['memory_limit']
            time_taken=json_data[index_mem]['time_taken']
        except:
            net_rx=0
            net_tx=0
            memory_limit=0
            net_tx
        stat = {
           "memory_limit": memory_limit,
           "time_taken": delta_time,
           "memory_usage": max_memory, # is not memory percentage
           "net_rx": net_rx,
           "net_tx": net_tx,
           "cpu_usage":cpu_total,  # is not cpu percentage
           "total_memory":total_memory
          }

        os.remove(res_file+".txt")     
        return stat
if __name__ == '__main__':
    orch = Orchestrator()
    orch.build("yolov3-object-detection")
    orch.build("cntk-image-recon")

    img = input("Enter image path (.png, .jpg, .jpeg): ")

    orch.call(img, "test")
