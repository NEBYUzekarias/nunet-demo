import os

"""
Get container name for docker purposes
"""
try:
    CONTAINER_NAME = os.environ['NUNET_CONTAINER_NAME']
except:
    CONTAINER_NAME = "nunet"    # default container name
