import React from "react";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

export default function NunetHeading({ icon: Icon, size, ...props }) {
    return (
        <Box mt={-1}>
            <Icon style={{ height: "4rem" }} />
            <Box mt={-1.5}>
                <Typography variant="h6" gutterBottom>
                    {props.children}
                </Typography>
            </Box>
        </Box>
    );
}
