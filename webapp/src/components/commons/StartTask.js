import React from "react";
import PlayIcon from "@material-ui/icons/PlayArrow";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

function StartTaskButton(props) {
    const goto_task_execute = function() {
        props.history.push("/execute");
    };

    return (
        <Box my={2}>
            <Button
                onClick={goto_task_execute}
                startIcon={<PlayIcon />}
                variant="contained"
                color="primary"
            >
                Start Task
            </Button>
        </Box>
    );
}

export default StartTaskButton;
