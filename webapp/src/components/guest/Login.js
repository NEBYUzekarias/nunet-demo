import React, { useState, useContext } from "react";
import { Link as RouterLink } from "react-router-dom";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";

import Copyright from "../commons/Copyright";
import { call_grpc, set_local_token } from "../utils/grpc";
import { SnackBarContext, CacheContext } from "../utils/contexts";
import get_device_info from "../utils/devices";

import Facebook from "./facebook";
import Google from "./google";

const { LoginInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    main: {
        paddingTop: theme.spacing(4)
    },
    paper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    logo: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),
        width: "100%"
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        padding: theme.spacing(0, 2)
    },
    submit: {
        margin: theme.spacing(3, 0, 1)
    }
}));

export default function SignIn(props) {
    const classes = useStyles();
    const theme = useTheme();
    const showSnackBar = useContext(SnackBarContext);
    const [cache, updateCache] = useContext(CacheContext);

    const nunet_logo =
        theme.palette.type === "dark"
            ? "logos/Nunet-text-dark-background.png"
            : "logos/Nunet-text-light-background.png";

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const changeUsing = valueSetter => event => {
        valueSetter(event.target.value.trim());
    };

    function login() {
        if (!email) {
            showSnackBar("Email can not be empty.");
            return;
        }
        if (!password) {
            showSnackBar("Password can not be empty.");
            return;
        }

        let request = new LoginInput();
        request.setEmail(email);
        request.setPassword(password);
        request.setDeviceName(get_device_info());

        call_grpc("login", request, handle_login_response, showSnackBar);
    }

    function handle_login_response(response) {
        let token = response.getAccessToken();
        let firstLogin = response.getBool();

        if (token) {
            set_local_token(token);
            updateCache({}, true); // clear cache

            // remove if error snackbar was shown
            showSnackBar("");

            // go to start page
            if (firstLogin) {
                props.history.push("/info");
            } else {
                props.history.push("/consumer");
            }
        } else {
            showSnackBar("Server not responding. Please, try again.");
        }
    }

    function onKeyPress(event) {
        if (event.key === "Enter") {
            login();
        }
    }

    return (
        <Container component="main" maxWidth="xs" className={classes.main}>
            <div className={classes.paper}>
                <img src={nunet_logo} alt="Logo" className={classes.logo} />
                <Typography component="h1" variant="h5">
                    Welcome
                </Typography>
                <form className={classes.form} noValidate>
                    <Box align="center" pt={2}>
                        <Typography>Login with</Typography>
                    </Box>

                    <Box py={1}>
                        <Divider />
                    </Box>

                    <Box>
                        <Google history={props.history} />
                        <Facebook history={props.history} />
                    </Box>

                    <Box py={1}>
                        <Divider />
                    </Box>
                    <TextField
                        value={email}
                        onChange={changeUsing(setEmail)}
                        onKeyPress={onKeyPress}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email"
                        name="email"
                        autoComplete="email"
                        autoFocus
                    />
                    <TextField
                        value={password}
                        onChange={changeUsing(setPassword)}
                        onKeyPress={onKeyPress}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={login}
                    >
                        Sign In
                    </Button>

                    {/* <Link href="#" variant="caption">
                        Forgot password?
                    </Link> */}

                    <Box pt={2}>
                        <Typography align="center">
                            Don't have an account?
                            <br />
                            <Link
                                component={RouterLink}
                                to="/signup"
                                variant="body1"
                            >
                                Join Now
                            </Link>
                        </Typography>
                    </Box>

                </form>
            </div>

            <Box mt={2}>
                <Copyright />
            </Box>
        </Container>
    );
}
