import React, { useContext } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import FacebookIcon from "mdi-material-ui/Facebook";

import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import { SnackBarContext } from "../utils/contexts";
import { call_grpc, set_local_token } from "../utils/grpc";
import get_device_info from "../utils/devices";

const { SmediaLoginInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    media: {
        margin: theme.spacing(1, 0, 1),
        color: "#4267b2",
        backgroundColor: theme.palette.common.white
    }
}));

export default function Facebook(props) {
    const classes = useStyles();
    const showSnackBar = useContext(SnackBarContext);

    const responseFacebook = response => {
        let token = response.accessToken;

        let request = new SmediaLoginInput();
        request.setMedia("facebook");
        request.setAccessToken(token);
        request.setDeviceName(get_device_info());

        call_grpc(
            "socialMediaLogin",
            request,
            handle_social_login_response,
            showSnackBar
        );
    };

    const handleFailure = error => {
        console.log("failure when facebook login:", error);
    };

    function handle_social_login_response(response) {
        let token = response.getAccessToken();
        let firstLogin = response.getBool();
        if (token) {
            set_local_token(token);

            // go to start page
            if (firstLogin) {
                props.history.push("/info");
            } else {
                props.history.push("/consumer");
            }
        } else {
            showSnackBar("Server not responding. Please, try again.");
        }
    }

    return (
        <FacebookLogin
            appId={process.env.REACT_APP_FACEBOOK_APPID}
            disableMobileRedirect={true}
            autoLoad={false}
            fields="name,email"
            callback={responseFacebook}
            render={renderProps => (
                <Button
                    onClick={renderProps.onClick}
                    variant="contained"
                    fullWidth
                    className={classes.media}
                    startIcon={<FacebookIcon />}
                >
                    FACEBOOK
                </Button>
            )}
            onFailure={handleFailure}
        />
    );
}
