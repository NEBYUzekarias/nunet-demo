import React, { useEffect, useState, useContext } from "react";
import TimeAgo from "react-timeago";

import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import AssignmentIcon from "@material-ui/icons/Assignment";
import HistoryIcon from "@material-ui/icons/History";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import ReplayIcon from "@material-ui/icons/Replay";

import { base64_src } from "../utils/images";
import { call_grpc } from "../utils/grpc";
import IconTypography from "../commons/IconTypography";
import Loading from "../commons/Loading";
import CardItem from "../commons/CardItem";
import StartTaskButton from "../commons/StartTask";
import ShowInfo from "../commons/ShowInfo";
import { CacheContext, SnackBarContext } from "../utils/contexts";

const { PageInput } = require("../../grpc/session_pb");

export default function PreviousTasks(props) {
  const itemsPerPage = 5;

  const showSnackBar = useContext(SnackBarContext);
  const [cache, updateCache] = useContext(CacheContext);

  const [loading, setLoading] = useState(false);

  const [tasks, setTasks] = useState(cache.tasks || null);
  const [noMoreTasks, setNoMoreTasks] = useState(false);

  const goto_result = (task_id, seconds, task_index) => () => {
    props.history.push({
      pathname: `/tasks/${task_id}`,
      timestamp: seconds,
      task_identifier: task_index
    });
  };

  function parseGrpcTask(task) {
    return {
      base64: task.getBase64(),
      result: task.getResult(),
      seconds: task.getSeconds(),
      task_id: task.getTaskId(),
      index: task.getIndex()
    };
  }

  const on_previous_tasks_response = function(response) {
    setLoading(false);
    if (response.getPrevioustasksList().length > 0) {
      let parsed_tasks = response.getPrevioustasksList().map(parseGrpcTask);
      setTasks(tasks => (tasks ? [...tasks, ...parsed_tasks] : parsed_tasks));
    } else if (tasks === null) {
      // user have no tasks i.e set empty array, but tasks stay null if error happens
      setTasks([]);
    } else {
      // this means all tasks for user are already loaded, nothing to load anymore
      setNoMoreTasks(true);
    }
  };

    function on_grpc_error(err) {
        setLoading(false);
    }

    function get_previous_tasks() {
        setLoading(true);
        let request = new PageInput();
        request.setOffset(tasks ? tasks.length : 0);
        request.setSize(itemsPerPage);

        call_grpc(
            "previousTasks",
            request,
            on_previous_tasks_response,
            showSnackBar,
            on_grpc_error
        );
    }

    useEffect(() => {
        if (!tasks) {
            get_previous_tasks();
        }

        return () => {
            updateCache({ tasks: tasks });
        };
    }, [tasks]);

  return (
    <React.Fragment>
      <IconTypography icon={HistoryIcon} variant="h5" gutterBottom>
        Previous Runs
      </IconTypography>

      {tasks &&
        Object.keys(tasks).map(index => (
          <CardItem
            key={index}
            image={base64_src(tasks[index].base64)}
            title={"Task #" + tasks[index].index}
            titleIcon={AssignmentIcon}
            subtitle={<TimeAgo date={tasks[index].seconds * 1000} />}
            subtitleIcon={AccessTimeIcon}
            onClick={goto_result(
              tasks[index].task_id,
              tasks[index].seconds,
              tasks[index].index
            )}
          />
        ))}

      {/* show loading indicator */}
      {loading && <Loading />}

      {!loading &&
        tasks &&
        tasks.length !== 0 &&
        tasks.length % itemsPerPage === 0 &&
        !noMoreTasks && (
          <Box m={2}>
            <Button
              startIcon={<ReplayIcon />}
              variant="contained"
              color="primary"
              onClick={get_previous_tasks}
            >
              Load more
            </Button>
          </Box>
        )}

      {!loading && tasks && tasks.length === 0 && (
        <React.Fragment>
          <Box mt={3}>
            <ShowInfo>You have not run any task yet!</ShowInfo>
            <StartTaskButton history={props.history} />
          </Box>
        </React.Fragment>
      )}
    </React.Fragment>
  );
}
