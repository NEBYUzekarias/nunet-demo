// to read image as base64
let file_reader = new FileReader();

// supported image types
let supported_files = ["image/jpeg", "image/jpg", "image/png"];

/**
 * Removes image type and format description from base64 string as used in img tag src property
 * @param {string} base64_string base64 image string as used in img tag src property
 */
function crop_mime(base64_string) {
    if (!base64_string) return null;

    let i = 0;
    while (base64_string[i] !== ",") {
        i++;
    }

    return base64_string.slice(i + 1);
}

/**
 * Check if selected file is valid for processing
 * @param {File} file input file selected by user
 */
function valid_file(file) {
    return supported_files.includes(file.type);
}

/**
 * Read input file as data_url(base64) as a promise
 * @param {input file} file read input file
 * @returns {promise} data_url result of the file as a promise
 */
function promiseFileAsDataURL(file) {
    return new Promise((resolve, reject) => {
        file_reader.onerror = () => {
            file_reader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        file_reader.onloadend = () => {
            resolve(file_reader.result);
        };

        file_reader.readAsDataURL(file);
    });
}

/**
 * Convert given image to base64 ready to be used for grpc purposes
 * @param {string || File} image image to be converted to base64 string with no mime start
 * @param {string} image_name name of image used when referring image in error messages
 */
async function get_image_base64(image, image_name, error_handler) {
    let image_base64 = null;

    if (typeof image === "string" || image instanceof String) {
        // assume it is base64 encoded image
        image_base64 = image;
    } else {
        // assume it is image from file input selected by user
        if (!valid_file(image)) {
            error_handler(
                `Unsupported file for ${image_name} image input selected.`
            );
        } else {
            try {
                image_base64 = await promiseFileAsDataURL(image);
            } catch (err) {
                error_handler(
                    `Error while reading ${image_name} image input. Check if appropriate file are selected.`
                );
                console.warn("error while base64 reading: ", err);
            }
        }
    }

    image_base64 = crop_mime(image_base64);

    return image_base64;
}

/**
 * Base64 image starting character for different formats based on their specification
 */
const JPG_START = "/9"; // only two character guaranteed for jpeg format
const PNG_START = "iVBORw0KGg"; // only 10 character guaranteed for png format

/**
 * MIME starting string for different image formats when used on src tag of an img element or else
 */
const JPG_MIME = "data:image/jpeg;base64,";
const PNG_MIME = "data:image/png;base64,";

/**
 * Create an image base64 string that can be used on src tag of an img element directly
 * @param {str} base64 : base64 encoded image string
 */
function base64_src(base64) {
    let mime = "";
    if (base64.startsWith(JPG_START)) {
        mime = JPG_MIME;
    } else if (base64.startsWith(PNG_START)) {
        mime = PNG_MIME;
    } else {
        console.warn("couldnot detect mime when trying to get src for base64");
    }

    return mime + base64;
}

export default get_image_base64;
export { base64_src };
