import React, { useState, useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Box from "@material-ui/core/Box";

import ProviderIcon from "../../icons/IconsNunet04";
import NunetHeading from "../commons/NunetHeading";
import PaperTable from "../commons/PaperTable";
import { call_grpc } from "../utils/grpc";
import { SnackBarContext } from "../utils/contexts";
import ShowInfo from "../commons/ShowInfo";
import Loading from "../commons/Loading";

const { Empty } = require("google-protobuf/google/protobuf/empty_pb.js");

const useStyles = makeStyles(theme => ({
    table_header: {
        fontWeight: theme.typography.fontWeightBold
    },
    cell_no_shrink: {
        width: "33.3%"
    }
}));

function ProviderPerspective(props) {
    const classes = useStyles();

    const showSnackBar = useContext(SnackBarContext);

    const [loading, setLoading] = useState(false);

    const [devices, setDevices] = useState(null);
    const [total, setTotal] = useState({
        tokenEarned: null,
        processCompleted: null
    });

    function on_grpc_error(err) {
        setLoading(false);
    }

    const on_provider_result = response => {
        setLoading(false);
        if (response.getDeviceList().length > 0) {
            let device_list = response.getDeviceList();
            setDevices(device_list);

            // calculate total
            let processCompleted = 0,
                tokenEarned = 0;
            for (let i in device_list) {
                processCompleted += device_list[i].getProcessCompleted();
                tokenEarned += device_list[i].getTokenEarned();
            }
            setTotal({
                tokenEarned: tokenEarned,
                processCompleted: processCompleted
            });
        } else if (devices === null) {
            // user have no devices, but devices stay null if error happens
            setDevices([]);
        }
    };

    const get_provider_devices = () => {
        setLoading(true);
        let request = new Empty();

        call_grpc(
            "provider",
            request,
            on_provider_result,
            showSnackBar,
            on_grpc_error
        );
    };

    useEffect(() => {
        if (!devices) {
            get_provider_devices();
        }
    }, [devices]);

    const goto_device = device_id => () => {
        props.history.push(`/device/${device_id}`);
    };

    return (
        <React.Fragment>
            <NunetHeading icon={ProviderIcon}>
                Provider Perspective
            </NunetHeading>

            {/* show loading indicator */}
            {loading && <Loading />}

            {devices && devices.length > 0 && (
                <PaperTable>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    align="center"
                                    className={classes.table_header}
                                >
                                    Your Resources
                                </TableCell>
                                <TableCell
                                    align="center"
                                    className={classes.table_header}
                                >
                                    Process completed
                                </TableCell>
                                <TableCell
                                    align="center"
                                    className={classes.table_header}
                                >
                                    NTX Earned
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {Object.keys(devices).map(index => (
                                <TableRow
                                    onClick={goto_device(
                                        devices[index].getDeviceName()
                                    )}
                                    key={index}
                                    hover={true}
                                >
                                    <TableCell
                                        align="center"
                                        className={classes.cell_no_shrink}
                                    >
                                        {devices[index].getDeviceName()}
                                    </TableCell>
                                    <TableCell align="center">
                                        {devices[index].getProcessCompleted()}
                                    </TableCell>
                                    <TableCell align="center">
                                        {devices[index].getTokenEarned()}
                                    </TableCell>
                                </TableRow>
                            ))}

                            <TableRow>
                                <TableCell
                                    align="center"
                                    className={classes.cell_no_shrink}
                                >
                                    Total
                                </TableCell>
                                <TableCell align="center">
                                    {total.processCompleted}
                                </TableCell>
                                <TableCell align="center">
                                    {total.tokenEarned}
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </PaperTable>
            )}

            {!loading && devices && devices.length === 0 && (
                <React.Fragment>
                    <Box mt={3}>
                        <ShowInfo>
                            No configured device found from provider!
                        </ShowInfo>
                    </Box>
                </React.Fragment>
            )}
        </React.Fragment>
    );
}

export default ProviderPerspective;
