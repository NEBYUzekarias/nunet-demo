import React from "react";

const SvgIconsnunet04 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-04_svg__a"
        x1={78.98}
        y1={146.54}
        x2={637.63}
        y2={629.06}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-04_svg__b"
        x1={88.39}
        y1={135.64}
        x2={647.04}
        y2={618.17}
        xlinkHref="#icons_nunet-04_svg__a"
      />
    </defs>
    <path
      d="M491.29 356.24L361.63 130.93C328 72.56 273 72.53 239.15 130.69L108.83 355.74c-33.73 58.15-6.27 106 61.12 106.08l259.92.41c67.39.08 95.02-47.61 61.42-105.99zm-113.08 40.07l-156.53-.24c-40.59-.05-57.12-28.87-36.81-63.89l78.48-135.54c20.36-35 53.53-35 73.77.15l43.15 75 6.66 11.57 28.27 49.12c20.24 35.16 3.6 63.88-36.99 63.83z"
      fill="url(#icons_nunet-04_svg__a)"
    />
    <path
      d="M376.15 325.9l-51.62-89.69c-13.37-23.24-35.3-23.25-48.75-.1L223.9 325.7c-13.43 23.16-2.5 42.21 24.33 42.24l103.47.16c26.83.03 37.83-18.96 24.45-42.2z"
      fill="url(#icons_nunet-04_svg__b)"
    />
  </svg>
);

export default SvgIconsnunet04;
