import React from "react";

const SvgIconsnunet06 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-06_svg__a"
        x1={110.78}
        y1={270.63}
        x2={489.22}
        y2={270.63}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#6fc9d6" />
        <stop offset={1} stopColor="#1182b5" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-06_svg__b"
        x1={139.89}
        y1={124.32}
        x2={492.1}
        y2={455.06}
        xlinkHref="#icons_nunet-06_svg__a"
      />
    </defs>
    <path
      d="M300 81.41c-104.5 0-189.22 84.71-189.22 189.22S195.5 459.85 300 459.85s189.22-84.72 189.22-189.22S404.5 81.41 300 81.41zm0 321.76a128.5 128.5 0 11128.5-128.5A128.5 128.5 0 01300 403.17z"
      fill="url(#icons_nunet-06_svg__a)"
    />
    <circle cx={300} cy={274.67} r={60.64} fill="url(#icons_nunet-06_svg__b)" />
  </svg>
);

export default SvgIconsnunet06;
