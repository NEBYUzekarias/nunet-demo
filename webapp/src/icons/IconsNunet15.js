import React from "react";

const SvgIconsnunet15 = props => (
  <svg
    id="icons_nunet-15_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-9"
        x1={439.77}
        y1={377.22}
        x2={450.28}
        y2={377.22}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-18"
        x1={109.9}
        y1={-52.89}
        x2={155.54}
        y2={637.16}
        xlinkHref="#icons_nunet-15_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-2"
        x1={272.53}
        y1={38.31}
        x2={234.19}
        y2={428.97}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-3"
        x1={261.28}
        y1={37.2}
        x2={222.94}
        y2={427.87}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-4"
        x1={344.36}
        y1={375.77}
        x2={402.53}
        y2={375.77}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#4ac2ba" />
        <stop offset={0.73} stopColor="#36a6ce" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-5"
        x1={454.36}
        y1={394.37}
        x2={469.44}
        y2={394.37}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-6"
        x1={456.97}
        y1={394.29}
        x2={466.85}
        y2={394.29}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-7"
        x1={441.4}
        y1={377.66}
        x2={448.64}
        y2={377.66}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-8"
        x1={437.48}
        y1={378.12}
        x2={452.57}
        y2={378.12}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient"
        x1={123.3}
        y1={-53.78}
        x2={168.94}
        y2={636.28}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.44} stopColor="#4ac2ba" />
        <stop offset={0.66} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-10"
        x1={457.9}
        y1={394.29}
        x2={465.91}
        y2={394.29}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-11"
        x1={437.48}
        y1={410.66}
        x2={452.57}
        y2={410.66}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-12"
        x1={440.82}
        y1={410.66}
        x2={449.62}
        y2={410.66}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-4"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-13"
        x1={305.15}
        y1={325.91}
        x2={350.78}
        y2={404.4}
        xlinkHref="#icons_nunet-15_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-14"
        x1={301.33}
        y1={339.45}
        x2={308.02}
        y2={809.07}
        xlinkHref="#icons_nunet-15_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-15"
        x1={333.99}
        y1={338.98}
        x2={340.68}
        y2={808.6}
        xlinkHref="#icons_nunet-15_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-16"
        x1={410.13}
        y1={337.9}
        x2={416.82}
        y2={807.52}
        xlinkHref="#icons_nunet-15_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-15_svg__linear-gradient-17"
        x1={443.97}
        y1={337.41}
        x2={450.66}
        y2={807.04}
        xlinkHref="#icons_nunet-15_svg__linear-gradient"
      />
      <style>{".icons_nunet-15_svg__cls-14{fill:#fff}"}</style>
    </defs>
    <path
      fill="url(#icons_nunet-15_svg__linear-gradient)"
      d="M123.93 211.95l26.05.03V93.22h-26.05v118.73z"
    />
    <path
      d="M245.87 455.44a151.31 151.31 0 00-1 18.67 35.79 35.79 0 003.92 16.89h-90.85V221.29l23.44 1.9 157.08 32.31v95.71a18.87 18.87 0 00-2.09 1.59h-11.05a15 15 0 00-5.75-3.85 45.53 45.53 0 00-34 .44 19.72 19.72 0 00-4.73 2.7 15.25 15.25 0 00-3.08 3.19 16.18 16.18 0 00-3.16 3.41c-1.67 2.43-3.28 4.88-4.89 7.34l-1.91 2.9a65 65 0 00-7.72 17.44c-3.07 10.18-5.82 21.29-8.42 34-2.07 10.3-4.42 22.54-5.79 35.07z"
      fill="url(#icons_nunet-15_svg__linear-gradient-2)"
    />
    <path
      fill="url(#icons_nunet-15_svg__linear-gradient-3)"
      d="M338.45 247.94V144.4L184.91 93.22h-26.97v118.77l24.33 2.11 156.18 33.84z"
    />
    <path
      d="M348.79 393.08h49.31a4.43 4.43 0 004.43-4.44v-25.75a4.44 4.44 0 00-4.43-4.44h-49.3a4.44 4.44 0 00-4.44 4.44v25.75a4.45 4.45 0 004.43 4.44z"
      fill="url(#icons_nunet-15_svg__linear-gradient-4)"
    />
    <circle
      cx={461.9}
      cy={394.37}
      r={7.54}
      fill="url(#icons_nunet-15_svg__linear-gradient-5)"
    />
    <path
      d="M461.9 389.35a4.94 4.94 0 105 4.93 4.93 4.93 0 00-5-4.93z"
      fill="url(#icons_nunet-15_svg__linear-gradient-6)"
    />
    <path
      fill="url(#icons_nunet-15_svg__linear-gradient-7)"
      d="M441.4 380.73h7.23l-3.61-6.13-3.62 6.13z"
    />
    <path
      d="M452.57 378.12a7.55 7.55 0 11-7.54-7.54 7.54 7.54 0 017.54 7.54z"
      fill="url(#icons_nunet-15_svg__linear-gradient-8)"
    />
    <path
      fill="url(#icons_nunet-15_svg__linear-gradient-9)"
      d="M450.28 381.66l-5.26-8.89-5.25 8.89h10.51z"
    />
    <path
      d="M457.9 394.28a4 4 0 104-4 4 4 0 00-4 4z"
      fill="url(#icons_nunet-15_svg__linear-gradient-10)"
    />
    <path
      d="M452.57 410.66a7.55 7.55 0 11-7.54-7.54 7.54 7.54 0 017.54 7.54z"
      fill="url(#icons_nunet-15_svg__linear-gradient-11)"
    />
    <path
      fill="url(#icons_nunet-15_svg__linear-gradient-12)"
      d="M446.08 410.66l3.54-3.54-.86-.86-3.54 3.54-3.54-3.54-.86.86 3.54 3.54-3.54 3.54.86.86 3.54-3.54 3.54 3.54.86-.86-3.54-3.54z"
    />
    <path
      d="M488 470.93c-.09 7.49-1.74 12.64-9 18.62-12.67 9.79-29.8 3.41-33.42-1.95s-6.72-13.68-9.17-20.9c-2-5.88-4-11.76-6-17.66-.62-1.88-2.19-2.58-3.79-3.28-.19-.08-.57.1-.8.26a23 23 0 01-28.32-.47 2.85 2.85 0 00-1.64-.57h-44.97a2.9 2.9 0 00-1.64.57A23 23 0 01321 446c-.23-.16-.6-.35-.8-.26-1.59.7-3.16 1.4-3.79 3.28-2 5.9-4 11.78-6 17.66-2.46 7.22-5.56 15.55-9.18 20.9s-20.75 11.74-33.43 1.95c-7.26-6-8.91-11.13-9-18.62a140.71 140.71 0 01.88-17.05c1.25-11.39 3.34-22.66 5.64-33.88 2.26-11 4.87-22 8.12-32.8a52 52 0 016.08-13.94c7.52-14.81 42.59-10.17 44.72-10.16h18l-.06 25.54a6.59 6.59 0 006.58 6.59h49.3a6.58 6.58 0 006.58-6.58V363.1h17.96c1.3 0 13.93-.73 24.35-.82a24.11 24.11 0 0120.29 10.88l.07.1a52.31 52.31 0 016.09 13.94c3.25 10.8 5.85 21.76 8.11 32.8 2.3 11.22 4.4 22.49 5.64 33.88a138.71 138.71 0 01.85 17.05z"
      fill="url(#icons_nunet-15_svg__linear-gradient-13)"
    />
    <path
      className="icons_nunet-15_svg__cls-14"
      d="M329.87 380.47a4.63 4.63 0 109.26 0V371a4.63 4.63 0 10-9.26 0z"
    />
    <path
      d="M328.1 398.3a25.93 25.93 0 10-25.93 25.92 25.93 25.93 0 0025.93-25.92z"
      fill="url(#icons_nunet-15_svg__linear-gradient-14)"
    />
    <path
      d="M348.8 433.14a13.47 13.47 0 10-13.47 13.47 13.47 13.47 0 0013.47-13.47z"
      fill="url(#icons_nunet-15_svg__linear-gradient-15)"
    />
    <path
      className="icons_nunet-15_svg__cls-14"
      d="M407.7 380.47a4.64 4.64 0 009.27 0V371a4.64 4.64 0 10-9.27 0z"
    />
    <path
      d="M425 433.14a13.47 13.47 0 10-13.47 13.47A13.47 13.47 0 00425 433.14z"
      fill="url(#icons_nunet-15_svg__linear-gradient-16)"
    />
    <path
      d="M470.72 394.82a25.93 25.93 0 10-25.93 25.93 25.93 25.93 0 0025.93-25.93z"
      fill="url(#icons_nunet-15_svg__linear-gradient-17)"
    />
    <path
      fill="url(#icons_nunet-15_svg__linear-gradient-18)"
      d="M123.93 221.29h26.05v269.78h-26.05z"
    />
    <rect
      className="icons_nunet-15_svg__cls-14"
      x={295.31}
      y={377.92}
      width={13.72}
      height={42.6}
      rx={4.82}
    />
    <rect
      className="icons_nunet-15_svg__cls-14"
      x={295.31}
      y={376.99}
      width={13.72}
      height={42.6}
      rx={4.82}
      transform="rotate(90 302.17 398.29)"
    />
    <rect
      className="icons_nunet-15_svg__cls-14"
      x={438.36}
      y={373.52}
      width={13.72}
      height={42.6}
      rx={4.82}
    />
    <rect
      className="icons_nunet-15_svg__cls-14"
      x={438.36}
      y={372.59}
      width={13.72}
      height={42.6}
      rx={4.82}
      transform="rotate(90 445.22 393.89)"
    />
    <path
      className="icons_nunet-15_svg__cls-14"
      d="M342.76 433.14a7.43 7.43 0 10-7.43 7.42 7.43 7.43 0 007.43-7.42zM418.91 433.14a7.43 7.43 0 10-7.42 7.42 7.43 7.43 0 007.42-7.42z"
    />
  </svg>
);

export default SvgIconsnunet15;
