import React from "react";

const SvgIconsnunet18 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-18_svg__a"
        x1={89.44}
        y1={473.87}
        x2={430.65}
        y2={491.94}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-18_svg__b"
        x1={265.03}
        y1={275.33}
        x2={279.01}
        y2={444.4}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#3eb3e6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-18_svg__c"
        x1={92.63}
        y1={395.54}
        x2={433.84}
        y2={413.6}
        xlinkHref="#icons_nunet-18_svg__a"
      />
      <linearGradient
        id="icons_nunet-18_svg__d"
        x1={265.03}
        y1={196.82}
        x2={279.01}
        y2={365.9}
        xlinkHref="#icons_nunet-18_svg__b"
      />
      <linearGradient
        id="icons_nunet-18_svg__e"
        x1={95.91}
        y1={315.08}
        x2={437.12}
        y2={333.14}
        xlinkHref="#icons_nunet-18_svg__a"
      />
      <linearGradient
        id="icons_nunet-18_svg__f"
        x1={265.03}
        y1={116.19}
        x2={279.01}
        y2={285.26}
        xlinkHref="#icons_nunet-18_svg__b"
      />
      <linearGradient
        id="icons_nunet-18_svg__g"
        x1={99.19}
        y1={234.61}
        x2={440.4}
        y2={252.67}
        xlinkHref="#icons_nunet-18_svg__a"
      />
      <linearGradient
        id="icons_nunet-18_svg__h"
        x1={313.99}
        y1={156.22}
        x2={221.26}
        y2={246.69}
        xlinkHref="#icons_nunet-18_svg__b"
      />
    </defs>
    <path
      d="M113.89 431.19s.37 68.88 1.08 70.69c9.41 23.93 78.76 42.5 162.93 42.5 85 0 154.94-18.95 163.21-43.23l.83-70z"
      fill="url(#icons_nunet-18_svg__a)"
    />
    <ellipse
      cx={277.92}
      cy={431.19}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-18_svg__b)"
    />
    <path
      d="M113.89 352.69s.37 68.87 1.08 70.69c9.41 23.92 78.76 42.5 162.93 42.5 85 0 154.94-19 163.21-43.23l.83-70z"
      fill="url(#icons_nunet-18_svg__c)"
    />
    <ellipse
      cx={277.92}
      cy={352.69}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-18_svg__d)"
    />
    <path
      d="M113.89 272.05s.37 68.88 1.08 70.69c9.41 23.92 78.76 42.5 162.93 42.5 85 0 154.94-18.95 163.21-43.23l.83-70z"
      fill="url(#icons_nunet-18_svg__e)"
    />
    <ellipse
      cx={277.92}
      cy={272.05}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-18_svg__f)"
    />
    <path
      d="M113.89 191.41s.37 68.88 1.08 70.69c9.41 23.93 78.76 42.5 162.93 42.5 85 0 154.94-18.95 163.21-43.23l.83-70z"
      fill="url(#icons_nunet-18_svg__g)"
    />
    <ellipse
      cx={277.92}
      cy={191.41}
      rx={164.03}
      ry={48.06}
      fill="url(#icons_nunet-18_svg__h)"
    />
  </svg>
);

export default SvgIconsnunet18;
