import React from "react";

const SvgIconsnunet16 = props => (
  <svg
    id="icons_nunet-16_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-10"
        x1={369.35}
        y1={161.82}
        x2={407.79}
        y2={161.82}
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-23"
        x1={1881.04}
        y1={224.76}
        x2={1940.19}
        y2={224.76}
        gradientTransform="matrix(-1 0 0 1 2258.85 0)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient"
        x1={184.6}
        y1={249.64}
        x2={219.62}
        y2={249.64}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-2"
        x1={190.63}
        y1={210.14}
        x2={188.3}
        y2={280.25}
        gradientTransform="rotate(13.22 189.262 251.407)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#4ac2ba" />
        <stop offset={0.73} stopColor="#36a6ce" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-22"
        x1={225.45}
        y1={227.18}
        x2={284.59}
        y2={227.18}
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-3"
        x1={1737.91}
        y1={249.64}
        x2={1772.92}
        y2={249.64}
        gradientTransform="matrix(-1 0 0 1 2158.85 0)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-4"
        x1={1743.94}
        y1={210.14}
        x2={1741.6}
        y2={280.25}
        gradientTransform="scale(-1 1) rotate(-76.7 667.801 1620.251)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-5"
        x1={297.31}
        y1={521.2}
        x2={315.52}
        y2={211.6}
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-6"
        x1={264.35}
        y1={544}
        x2={266.24}
        y2={370.56}
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-7"
        x1={342.97}
        y1={544.85}
        x2={344.86}
        y2={371.41}
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-21"
        x1={298.32}
        y1={123.59}
        x2={298.83}
        y2={162.11}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#4ac2ba" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-8"
        x1={304.49}
        y1={335.52}
        x2={297.37}
        y2={86.33}
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-9"
        x1={345.24}
        y1={124.14}
        x2={445.65}
        y2={311.44}
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-20"
        x1={298.9}
        y1={157.19}
        x2={298.08}
        y2={128.51}
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-11"
        x1={410.56}
        y1={120.83}
        x2={398.42}
        y2={155.13}
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-12"
        x1={1891}
        y1={127.5}
        x2={1991.4}
        y2={314.79}
        gradientTransform="matrix(-1 0 0 1 2145.43 0)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-13"
        x1={1915.11}
        y1={165.17}
        x2={1953.54}
        y2={165.17}
        gradientTransform="matrix(-1 0 0 1 2145.43 0)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-14"
        x1={1956.31}
        y1={124.18}
        x2={1944.18}
        y2={158.49}
        gradientTransform="matrix(-1 0 0 1 2145.43 0)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient-2"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-15"
        x1={296.33}
        y1={308.29}
        x2={304.45}
        y2={170.29}
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-16"
        x1={302.74}
        y1={307.35}
        x2={303.45}
        y2={349.12}
        gradientTransform="matrix(1 0 0 -1.01 0 644.32)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-17"
        x1={304.12}
        y1={317.75}
        x2={304.84}
        y2={359.77}
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-18"
        x1={200.67}
        y1={307.89}
        x2={250.22}
        y2={307.89}
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-16_svg__linear-gradient-19"
        x1={1731.66}
        y1={237.65}
        x2={1781.21}
        y2={237.65}
        gradientTransform="matrix(-1 .09 .09 1 2109.71 -82.31)"
        xlinkHref="#icons_nunet-16_svg__linear-gradient"
      />
      <clipPath id="icons_nunet-16_svg__clip-path">
        <path
          className="icons_nunet-16_svg__cls-1"
          d="M205.17 209.26s-18.06-.41-20.07 5.09.53 74.34 34.52 75.67z"
        />
      </clipPath>
      <clipPath id="icons_nunet-16_svg__clip-path-3">
        <path
          className="icons_nunet-16_svg__cls-1"
          d="M202.23 206.92a88.25 88.25 0 011.56-21.92c5.51-27.35 25.8-88 97.2-84.14 96.53 5.29 98.19 97.14 98.19 97.14s3.82 100.3-25.36 140.65c-28.9 39.91-44.65 44.59-69.45 47.77-16.14 2.07-41.63-19.47-57.46-34.76a102.64 102.64 0 01-25-37.9c-7.67-20.63-17.07-55.52-19.68-106.84z"
        />
      </clipPath>
      <clipPath id="icons_nunet-16_svg__clip-path-2">
        <path
          className="icons_nunet-16_svg__cls-1"
          d="M400.37 209.26s18.07-.41 20.07 5.09-.53 74.34-34.51 75.67z"
        />
      </clipPath>
      <style>{".icons_nunet-16_svg__cls-1{fill:none}"}</style>
    </defs>
    <g clipPath="url(#icons_nunet-16_svg__clip-path)">
      <path
        d="M205.17 209.26s-18.06-.41-20.07 5.09.53 74.34 34.52 75.67z"
        fill="url(#icons_nunet-16_svg__linear-gradient)"
      />
      <ellipse
        cx={189.26}
        cy={251.42}
        rx={12.85}
        ry={38.6}
        transform="rotate(-13.22 189.24 251.41)"
        fill="url(#icons_nunet-16_svg__linear-gradient-2)"
      />
    </g>
    <g clipPath="url(#icons_nunet-16_svg__clip-path-2)">
      <path
        d="M400.37 209.26s18.07-.41 20.07 5.09-.53 74.34-34.51 75.67z"
        fill="url(#icons_nunet-16_svg__linear-gradient-3)"
      />
      <ellipse
        cx={416.28}
        cy={251.42}
        rx={38.6}
        ry={12.85}
        transform="rotate(-76.78 416.295 251.425)"
        fill="url(#icons_nunet-16_svg__linear-gradient-4)"
      />
    </g>
    <path
      d="M303.21 498.22l109.15-49.45a7.27 7.27 0 00.39-13c-16.46-8.65-43.19-24.5-53-40.61-14.61-23.95 11-59.65 11-59.65l-66.21 27.7-71.28-27.7s25.58 35.7 11 59.65c-9.82 16.11-36.55 32-53 40.61a7.26 7.26 0 00.39 13l109.17 49.46v1.11l1.23-.56 1.23.56z"
      fill="url(#icons_nunet-16_svg__linear-gradient-5)"
    />
    <path
      d="M269.92 471c1.46 6.36 3 12.94 7.06 18.07s11.27 8.35 17.24 5.73l-19.47-111.32c-1-5.88-2.11-11.91-5.23-17-7.43-12.11-24.16-15.63-32.27-27.29C239 345 260.94 432 269.92 471z"
      fill="url(#icons_nunet-16_svg__linear-gradient-6)"
    />
    <path
      d="M339.77 471c-1.47 6.36-3 12.94-7.07 18.07s-10.41 5.86-16.38 3.24c6.49-37.1 12.12-71.7 18.61-108.8 1-5.88 2.12-11.91 5.24-17 7.43-12.11 24.15-15.63 32.27-27.29"
      fill="url(#icons_nunet-16_svg__linear-gradient-7)"
    />
    <g clipPath="url(#icons_nunet-16_svg__clip-path-3)">
      <path
        d="M202.23 206.92a88.25 88.25 0 011.56-21.92c5.51-27.35 25.8-88 97.2-84.14 96.53 5.29 98.19 97.14 98.19 97.14s3.82 100.3-25.36 140.65c-28.9 39.91-44.65 44.59-69.45 47.77-16.14 2.07-41.63-19.47-57.46-34.76a102.64 102.64 0 01-25-37.9c-7.67-20.63-17.07-55.52-19.68-106.84z"
        fill="url(#icons_nunet-16_svg__linear-gradient-8)"
      />
      <path
        d="M335.63 105.84A135.59 135.59 0 01348.19 181c-.79 6.51-1.68 14.14 3.08 18.64 3.34 3.16 8.35 3.57 13 3.8l37.15 1.87a87.28 87.28 0 00-66-101.86"
        fill="url(#icons_nunet-16_svg__linear-gradient-9)"
      />
      <path
        d="M381.48 128.91A38.09 38.09 0 00403 194.72a49.33 49.33 0 00-20.19-64.12"
        fill="url(#icons_nunet-16_svg__linear-gradient-10)"
      />
      <path
        d="M388.13 126.79a38.09 38.09 0 0021.55 65.81 49.33 49.33 0 00-20.18-64.12"
        fill="url(#icons_nunet-16_svg__linear-gradient-11)"
      />
      <path
        className="icons_nunet-16_svg__cls-1"
        d="M202.23 211a95.07 95.07 0 011.56-22.78c5.51-28.41 25.8-91.43 97.2-87.37 96.53 5.49 98.19 100.86 98.19 100.86s3.82 104.18-25.36 146.07c-28.9 41.45-44.65 46.3-69.45 49.61-16.14 2.15-41.63-20.22-57.46-36.1a107 107 0 01-25-39.35c-7.67-21.42-17.07-57.65-19.68-110.94z"
      />
      <path
        d="M264.05 109.2a135.56 135.56 0 00-12.56 75.13c.79 6.5 1.68 14.13-3.08 18.63-3.34 3.16-8.36 3.57-12.95 3.8l-37.16 1.88a87.29 87.29 0 0166-101.87"
        fill="url(#icons_nunet-16_svg__linear-gradient-12)"
      />
      <path
        d="M218.2 132.26a38.09 38.09 0 01-21.55 65.81A49.33 49.33 0 01216.84 134"
        fill="url(#icons_nunet-16_svg__linear-gradient-13)"
      />
      <path
        d="M211.54 130.15A38.09 38.09 0 01190 196a49.34 49.34 0 0120.18-64.13"
        fill="url(#icons_nunet-16_svg__linear-gradient-14)"
      />
      <path
        d="M298 223.67a154.69 154.69 0 017.1 61.65c-.29 3.26-.73 6.64-2.53 9.37s-5.36 4.56-8.41 3.38c-2.68-1-4.13-4.08-4.23-6.95s.9-5.66 1.79-8.39c6.21-18.85 7.73-39.27 6.28-59.06"
        fill="url(#icons_nunet-16_svg__linear-gradient-15)"
      />
      <path
        d="M273.84 326.67l20-4.64a10.69 10.69 0 015.8.26l.88.29a10.69 10.69 0 007.23-.18 10.81 10.81 0 016.9-.28l17.21 5s-25.86 7.05-58.02-.45z"
        fill="url(#icons_nunet-16_svg__linear-gradient-16)"
      />
      <path
        d="M295.57 341.36h17.9l-3 3.59a7.89 7.89 0 01-12.35-.24z"
        fill="url(#icons_nunet-16_svg__linear-gradient-17)"
      />
      <path
        d="M200.67 253.93A103.76 103.76 0 01248 364c-32.18-27-49.81-70.43-45.58-112.23"
        fill="url(#icons_nunet-16_svg__linear-gradient-18)"
      />
      <path
        d="M400.55 251.09A103.79 103.79 0 00363 364.86c29.71-29.71 43.5-74.49 35.66-115.76"
        fill="url(#icons_nunet-16_svg__linear-gradient-19)"
      />
      <ellipse
        cx={298.6}
        cy={146.6}
        rx={21.91}
        ry={16.45}
        fill="url(#icons_nunet-16_svg__linear-gradient-20)"
      />
      <ellipse
        cx={298.6}
        cy={144.65}
        rx={17.38}
        ry={13.06}
        fill="url(#icons_nunet-16_svg__linear-gradient-21)"
      />
      <path
        d="M254.45 223.94a43.1 43.1 0 00-21 6.75 35.62 35.62 0 0128.15-17.05 25.09 25.09 0 00-21.64 1c-6.55 3.59-13.2 21.24-14.52 22.64a123.12 123.12 0 0116-4c1.74-.31 4.64-1.41 6.17-.27s2.06 4.5 3.22 6a8.6 8.6 0 005.67 3.57 8.21 8.21 0 007.07-3.35c1.32-1.69 1.74-3.53 2.51-5.46 1.07-2.65 1.39-2.16 4.48-1.86a88.21 88.21 0 0114.07 2.49 43.14 43.14 0 00-30.18-10.46z"
        fill="url(#icons_nunet-16_svg__linear-gradient-22)"
      />
      <path
        d="M348.8 221.53a43.09 43.09 0 0121 6.74 35.63 35.63 0 00-28.15-17.05 25.05 25.05 0 0121.64 1c6.55 3.59 13.21 21.24 14.53 22.64a122.06 122.06 0 00-16-4c-1.74-.31-4.64-1.41-6.17-.27s-2.06 4.49-3.22 6a8.65 8.65 0 01-5.67 3.58 8.21 8.21 0 01-7.07-3.35c-1.32-1.69-1.74-3.53-2.51-5.46-1.07-2.65-1.38-2.16-4.48-1.86a88.41 88.41 0 00-14.07 2.49 43.09 43.09 0 0130.17-10.46z"
        fill="url(#icons_nunet-16_svg__linear-gradient-23)"
      />
    </g>
    <ellipse
      cx={298.08}
      cy={144.82}
      rx={10.61}
      ry={7.68}
      fill="#00cfff"
      opacity={0.46}
    />
  </svg>
);

export default SvgIconsnunet16;
