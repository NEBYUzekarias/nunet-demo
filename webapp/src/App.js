import React, { useState } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import CssBaseline from "@material-ui/core/CssBaseline";

import Login from "./components/guest/Login";
import Signup from "./components/guest/Signup";
import Layout from "./components/commons/Layout";
import SnackBar from "./components/commons/SnackBar";
import TaskExecute from "./components/consumers/TaskExecute";
import PreviousTasks from "./components/consumers/PreviousTasks";
import TaskResult from "./components/consumers/TaskResult";
import RewardTable from "./components/consumers/RewardTable";
import ConsumerPerspective from "./components/consumers/ConsumerPerspective";
import ProviderPerspective from "./components/providers/ProviderPerspective";
import DeviceDetails from "./components/providers/DeviceDetails";
import TaskInformation from "./components/consumers/TaskInformation";

import { CacheContext, SnackBarContext } from "./components/utils/contexts";
import { is_logged_in } from "./components/utils/grpc";

// nunet branding theme styles(taken from nunet brand guidelines and structured for mui theme system)

// don't forget to link css for nunito, hind and robot font in index.html
const nunito_font = {
    fontFamily: '"Nunito", "Roboto", "Helvetica", "Arial", sans-serif'
};
const roboto_condensed = {
    fontFamily: '"Roboto Condensed", "Helvetica", "Arial", sans-serif'
};
// const roboto_font = {
//     fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
// };
// // hindfont is taken from singularitynet website buttons, not documented on the brand guidelines
// const hind_font = {
//     fontFamily: '"Hind", "Roboto Condensed", "Helvetica", "Arial", sans-serif'
// };

// primary and secondary colors are too many in brand guidelines, so
// I have choosen them based on my opinion what fits to mui theme system
const primary_colors = {
    main: "#2e9ad6" // bluish
};

const secondary_colors = {
    light: "#d0eaf6", // white little greenish
    main: "#00a79d" // greenish
    // dark: "#0a2042" // dark bluebalckish
};

// common theme styles for dark and white theme based on mui theme system
const common_theme = {
    palette: {
        primary: primary_colors,
        secondary: secondary_colors
    },
    typography: {
        fontFamily: roboto_condensed.fontFamily,
        body1: nunito_font,
        body2: nunito_font,
        button: roboto_condensed,
        caption: nunito_font
    }
};

// nunet branding theme(taken from nunet brand guidelines)
const light_theme = createMuiTheme({
    palette: {
        ...common_theme.palette,
        background: {
            default: "#f5f5f5" // card and papers are not standing out so little darker
        }
    },
    typography: common_theme.typography
});

// nunet branding dark theme(taken from nunet brand guidelines)
const dark_theme = createMuiTheme({
    palette: {
        type: "dark",
        ...common_theme.palette,
        background: {
            default: "#0a2042", // dark blueblackish
            paper: "#233655" // calculated lighter shade of above default
            // paper: "#3b4d68", // calculated more lighter shade of above default
        }
    },
    typography: common_theme.typography
});

// available themes
const available_themes = {
    light: light_theme,
    dark: dark_theme
};

function App() {
    // app constants
    const DEFAULT_THEME = dark_theme;
    const DEFAULT_NOTIFICATION_VARIANT = "error"; // default error notifications

    // app cache storage
    const [cache, setCache] = useState({});

    /**
     * a handy interface to update cache, will expose this interface but not setCache to child components
     * @param {object} object: an object to update the current cache based on the key of the object
     * @param {boolean} clear: if true clears cache before setting the new object in cache
     */
    const updateCache = function(object, clear = false) {
        if (clear) {
            setCache(cache => ({ ...object }));
        } else {
            setCache(cache => ({ ...cache, ...object }));
        }
    };

    // app global state storage
    const [store, setStore] = useState({});

    /**
     * a handy interface to update store state, will expose this interface but not setStore to child components
     * @param {str} key: the key of the store object to be updated
     * @param {object} object: an object to update the current cache
     * @param {boolean} clear: if true clears store before setting the new object in cache
     */
    const updateStore = function(key, valueUpdater, clear = false) {
        if (clear) {
            setStore(store => ({ [key]: valueUpdater(store[key]) }));
        } else {
            setStore(store => ({ ...store, [key]: valueUpdater(store[key]) }));
        }
    };

    // set preferred theme or the default one other wise
    const preferred_theme = localStorage.getItem("theme");
    const [theme, setTheme] = useState(
        available_themes[preferred_theme]
            ? available_themes[preferred_theme]
            : DEFAULT_THEME
    );

    // notification states

    // notification message texts(also if falsy no notifications)
    const [message, setMessage] = useState("");
    // variant could be a string of: error | warning | success | info
    const [variant, setVariant] = useState(DEFAULT_NOTIFICATION_VARIANT);

    /**
     * Closes snackbar notifications
     */
    function closeSnackBar() {
        setMessage("");
    }

    /**
     * Shows snackbar of message based on type of message
     * @param {str} message : a text message to be shown on snackbar
     * @param {str} variant : type of message: error | info | warn | success
     */
    function showSnackBar(message, variant) {
        setMessage(message);
        if (variant) {
            setVariant(variant);
        } else {
            setVariant(DEFAULT_NOTIFICATION_VARIANT);
        }
    }

    /**
     * Change type of theme between dark mode and light mode
     * and save it as preferred theme for later use
     */
    function toggleTheme() {
        if (theme === dark_theme) {
            setTheme(light_theme);
            localStorage.setItem("theme", "light");
        } else {
            setTheme(dark_theme);
            localStorage.setItem("theme", "dark");
        }
    }

    return (
        <CacheContext.Provider value={[cache, updateCache]}>
            <MuiThemeProvider theme={theme}>
                <CssBaseline />

                {/* All pages will be put here based on their url */}
                <SnackBarContext.Provider value={showSnackBar}>
                    <Router>
                        <Switch>
                            <Route exact path="/">
                                {is_logged_in() ? (
                                    <Redirect to="/consumer" />
                                ) : (
                                    <Redirect to="/login" />
                                )}
                            </Route>

                            <Layout
                                path="/info"
                                component={TaskInformation}
                                toggleTheme={toggleTheme}
                            />
                            <Route
                                path="/login"
                                render={props => <Login {...props} />}
                            />
                            <Route
                                path="/signup"
                                render={props => <Signup {...props} />}
                            />
                            <Layout
                                path="/execute"
                                component={TaskExecute}
                                toggleTheme={toggleTheme}
                            />
                            <Layout
                                exact
                                path="/tasks"
                                component={PreviousTasks}
                                toggleTheme={toggleTheme}
                            />
                            <Layout
                                exact
                                subpage
                                path="/tasks/:task_id"
                                component={TaskResult}
                                toggleTheme={toggleTheme}
                            />
                            <Layout
                                path="/rewards"
                                component={RewardTable}
                                toggleTheme={toggleTheme}
                            />
                            <Layout
                                path="/provider"
                                component={ProviderPerspective}
                                toggleTheme={toggleTheme}
                            />
                            <Layout
                                subpage
                                path="/device/:device_name"
                                component={DeviceDetails}
                                toggleTheme={toggleTheme}
                            />
                            <Layout
                                path="/consumer"
                                component={ConsumerPerspective}
                                toggleTheme={toggleTheme}
                            />
                        </Switch>
                    </Router>
                </SnackBarContext.Provider>

                <SnackBar
                    message={message}
                    closeSnackBar={closeSnackBar}
                    variant={variant}
                />
            </MuiThemeProvider>
        </CacheContext.Provider>
    );
}

export default App;
