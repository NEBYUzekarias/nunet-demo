FROM node:latest AS builder

COPY . /webapp

# change the working directory
WORKDIR /webapp

# docker-compose will not let you get environment variables on build time and build args as environment variables
# these args are used in npm build as environment variable(alas the weird variable setting code)
ARG GRPC_PORT
ENV GRPC_PORT ${GRPC_PORT}
ARG FACEBOOK_APPID
ENV FACEBOOK_APPID ${FACEBOOK_APPID}
ARG GOOGLE_CLIENT
ENV GOOGLE_CLIENT ${GOOGLE_CLIENT}

RUN npm install --silent
RUN npm run build

# start secure nginx server using new build
FROM nginx:latest
COPY --from=builder /webapp/build /var/www/build
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 443

# start nginx with wierd command
CMD ["nginx", "-g", "daemon off;"]
